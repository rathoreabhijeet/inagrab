angular.module('starter')

  .filter('capitalize', function () {
    return function (input, all) {
      return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }) : '';
    }
  })

  .filter('catFilter', function () {
    return function (x, searchText) {
      if (x) {
        var filtered = [];
        var letterMatch = new RegExp("\\b" + searchText, 'i');
        for (var i = 0; i < x.length; i++) {
          var item = x[i];
          n = item.length;
          if (letterMatch.test(item.name.substring(0, n)) || item.name.match(searchText)) {
            filtered.push(item);
          }
        }
        return filtered;

      }

    };
  })