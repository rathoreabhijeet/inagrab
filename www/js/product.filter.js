angular.module('starter')
  .filter('productFilter', function ($rootScope, $http) {
    return function (inputArray) {
      if (inputArray) {
        if($rootScope.rating) {
          inputArray = _.filter(inputArray, function (product) {
            if (parseInt(product.average_rating) <= parseInt($rootScope.rating)) {
              return true
            }
          });
        }
        if($rootScope.review) {
          inputArray = _.filter(inputArray, function (product) {
            if (parseInt(product.rating_count) <= parseInt($rootScope.review)) {
              return true
            }
          });
        }
        if($rootScope.price) {
          inputArray = _.filter(inputArray, function (product) {
            if (parseInt(product.price) <= parseInt($rootScope.price)) {
              return true
            }
          });
        }
        if($rootScope.website && $rootScope.website !== 'All') {
          inputArray = _.filter(inputArray, function (product) {
            if (product.websiteArray && product.websiteArray.length) {
              var matchWebsite = false;
              for(var i=0; i<product.websiteArray.length; i++) {
                if((product.websiteArray[i].domain.toLowerCase().split('.')[0]) === ($rootScope.website.toLowerCase())) {
                  matchWebsite = true;
                  break;
                }
              }
              if(matchWebsite) {
                return true;
              }
            }
          });
        }
        $rootScope.noProductDataAfterFilter = inputArray.length ? false : true;
        return inputArray;
      }

    }
  });
