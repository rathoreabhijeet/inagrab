angular.module('starter')
  .controller('reviewsCtrl', reviewsCtrl)
function reviewsCtrl($window, $stateParams, $state, $sce, $timeout) {
  var reviews = this;
  //variables for StarRating

  var devWidth = $window.innerWidth;
  reviews.deviceHeight = $window.innerHeight;
  reviews.fullHeight = { 'height': reviews.deviceHeight + 'px', width: devWidth + 'px' };

  // reviews.iframe = $stateParams.link;
  reviews.iframe = $sce.trustAsResourceUrl($stateParams.link);
  $timeout(function(){
    reviews.show=true;
  },500)

  console.log(reviews.iframe);
  reviews.goBack = function () {
    $state.go('productDetail');
  }
}
