angular.module('starter')
  .controller('cartCtrl',cartCtrl);

function cartCtrl($state,Shop,$scope,$timeout,$rootScope,AuthService,Flags,$stateParams, paramService){
  console.log("cart ctrl is working");
  var cart=this;

  $timeout(function(){
    cart.cartItems =  JSON.parse(window.localStorage.getItem(Shop.name+"-cart"));
    console.log(cart.cartItems);
  },10)


  $scope.data = {
    showDelete: false
  };

  $scope.addItem = function(x, qty){
    for(i in cart.cartItems){
      if(cart.cartItems[i].id == x.id){
        exist = true;
        cart.cartItems[i].qty = cart.cartItems[i].qty+qty;

        if(cart.cartItems[i].qty==0){
          cart.cartItems.splice(i, 1);
        }
        break;
      }
    }
    window.localStorage.setItem(Shop.name+"-cart", JSON.stringify(cart.cartItems));
    $scope.updateCart();
  };

  $scope.getCartItems = function(){
    var tmp=0;
    for(i in cart.cartItems)
      tmp+=cart.cartItems[i].qty;
    return tmp;
  }

  $scope.getCartTotal=function(){
    var tmp=0;
    for(i in cart.cartItems)
      tmp+=cart.cartItems[i].qty*cart.cartItems[i].price;
    return tmp;
  };

  $scope.removeCart = function(i, x){
    cart.cartItems.splice(i, 1);
    window.localStorage.setItem(Shop.name+"-cart", JSON.stringify(cart.cartItems));
    $scope.updateCart();
  }
  $scope.updateCart = function(){
    var cart = window.localStorage.getItem(Shop.name+"-cart") ? JSON.parse(window.localStorage.getItem(Shop.name+"-cart")) :'';
    if(cart.length>0){
      $scope.totalCartItem = 0;
      for(var i in cart)
        $scope.totalCartItem += cart[i].qty;
    }else  $scope.totalCartItem = 0;
  };

  cart.GoToShippingDetails = function(){
    var userInfo = JSON.parse(window.localStorage.getItem(Shop.name+"-user"));
    console.log(userInfo);
    if(userInfo){
      $state.go('shippingAddress');
    }
    else {
      Flags.redirectFormCart=1;
      $state.go('login');
    }

  }
  cart.GoToCategory = function(){
    $state.go('category');
  }
  cart.goBack = function(){
    if($stateParams.prevState) {
      $state.go($stateParams.prevState);
    }
    else{
      $state.go('category');
    }
  }
  cart.GoToUserProfile = function(){
    console.log($rootScope.rangeData);
    var user = window.localStorage.getItem(Shop.name+"-user");
    if(AuthService.isAuthenticated()&&user){
      $state.go('userProfile');
    }
    else {
      if (window.cordova) {
        var onConfirm = function (buttonIndex) {

          if (buttonIndex == 1) {
            paramService.signin={};
            paramService.signin.prevState = 'category';
            $state.go('signIn');
          }
          else {
            console.log('dismissed');
          }
        };
        navigator.notification.confirm(
          'Only logged in users can see their profile', // message
          onConfirm,            // callback to invoke with index of button pressed
          'Not logged in !',           // title
          ['Login', 'Dismiss']     // buttonLabels
        );
      }
      else {
        var r = confirm('Only logged in users can see their profile. Want to login?');
        if (r == true) {
          paramService.signin={};
          paramService.signin.prevState = 'category';
            $state.go('signIn');

        } else {
          console.log('dismissed');
        }
      }
    }
  }
}
