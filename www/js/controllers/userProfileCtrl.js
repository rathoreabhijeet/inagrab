angular.module('starter')
  .controller('userProfileCtrl', userProfileCtrl);

function userProfileCtrl($window, $state, Shop, $localForage, $scope, $ionicLoading, $timeout, $rootScope, paramService) {
  console.log("userProfile ctrl is working");
  var userProfile = this;
  var deviceWidtht = $window.innerWidth;
  var deviceHeight = $window.innerHeight;
  console.log("this is for random search of products");
  userProfile.currencies = [];
  var usedCurrencies = ['GBP', 'EUR', 'JPY', 'USD', 'INR', 'CAD', 'RUB']
  userProfile.selectedCurrency = paramService.selectedCurrency;
  function getCurrencyName(code) {
    switch (code) {
      case 'GBP':
        return 'Great Britain Pound';
      case 'EUR':
        return 'Euro';
      case 'JPY':
        return 'Yen';
      case 'INR':
        return 'Indian Rupee';
      case 'CAD':
        return 'Canadian Dollar';
      case 'RUB':
        return 'Russian Ruble';
    }
  }
  function getCurrencySymbol(code) {
    switch (code) {
      case 'GBP':
        return '£';
      case 'EUR':
        return '€';
      case 'JPY':
        return '¥';
      case 'INR':
        return 'Rs.';
      case 'CAD':
        return 'CA$';
      case 'RUB':
        return 'руб.';
    }
  }

  _.each(usedCurrencies, function (currency) {
    if (currency == 'USD') {
      userProfile.currencies.push({
        'currencyCode': 'USD', 'rate': 1,
        'name': 'US Dollar',
        'symbol': '$'
      });
    }
    else {
      userProfile.currencies.push({
        'currencyCode': currency, 'rate': parseFloat($rootScope.currencyConversionData[currency]),
        'name': getCurrencyName(currency), 'symbol': getCurrencySymbol(currency)
      });
    }
  })
  console.log(userProfile.currencies);

  userProfile.selectCurrency = function (currency) {
    userProfile.selectedCurrency = currency;
    paramService.selectedCurrency = currency;
    console.log(currency);
    paramService.priceFilter.max = Math.round(paramService.priceFilter.fixed_max*currency.rate);
    paramService.priceFilter.min = Math.round(paramService.priceFilter.fixed_min*currency.rate);
    userProfile.showCurrencyList = false;
  }

  userProfile.toggleCategoryList = function () {
    userProfile.showCurrencyList = !userProfile.showCurrencyList;
  }
  userProfile.hideCategoryList = function () {
    userProfile.showCurrencyList = false;
  }

  userProfile.userInfo = JSON.parse(window.localStorage.getItem(Shop.name + "-user"));
  console.log(userProfile.userInfo);
  $localForage.getItem('userloginInfo').then(function (data) {
    if (data) {
      userProfile.userInfo.image = data.picture;
      console.log(userProfile.userInfo);
    }
  })
  $localForage.getItem('userInfoByGoogleLogin').then(function (data) {
    if (data) {
      userProfile.userInfo = data;
      console.log(userProfile.userInfo);
    }
  })
  userProfile.GoBack = function () {
    $state.go('category');
  }
  userProfile.InviteFriend = function () {
    $state.go('invite');
  }
  userProfile.orderHistory = function () {
    $state.go('orders');
  }
  userProfile.logout = function () {

    if (window.localStorage.getItem(Shop.name + "-user")) {
      authToken = undefined;
      id = '';
      username = '';
      email = '';
      name = '';
      isAuthenticated = false;
      //$http.defaults.headers.common['X-Auth-Token'] = undefined;
      $localForage.clear();

      window.localStorage.clear();
      $scope.showSuccess("User logged out successfully");
      console.log("user logout");
      $state.go('login');
    }
  }
  $scope.showSuccess = function (x) {
    $ionicLoading.show({
      template: '<div class="info">' + x + '</div>'
    });
    $timeout(function () {
      $ionicLoading.hide();
    }, 1000);
  };
}
