angular.module('starter')
  .controller('signInCtrl',signInCtrl);

function signInCtrl($state,signInData,$scope,$rootScope,AuthService,$ionicLoading, $stateParams, Flags, paramService){
  console.log("category ctrl is working");
  var signIn=this;
  $rootScope.user={};

  signIn.GoToForgotPassword = function(){
    $state.go('forgotPassword');
  }
  signIn.goToSignUp = function(){
    $state.go('login',{prevState:'signIn'});
  }
  signIn.GoBack = function(){
      $state.go(paramService.signin.prevState);    
  }
  signIn.UserSignin = function(){
    $ionicLoading.show();
    console.log(signInData);
    //console.log($scope.user);
    console.log($rootScope.user);

    AuthService.login($rootScope.user).then(function(x){
      console.log(x);
      console.log("successfuly login");
      $ionicLoading.hide();
      if(Flags.redirectFormCart){
        $state.go('shippingAddress');
      }
      else {
        $state.go('category');
      }
    }, function(err){
      console.log(err);
      $scope.message = err;
      $ionicLoading.hide();
    });
    //$state.go('category');
  }
}
