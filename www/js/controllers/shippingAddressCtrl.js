angular.module('starter')
  .controller('shippingAddressCtrl',shippingAddressCtrl);

function shippingAddressCtrl($ionicModal,$scope,$rootScope,$ionicLoading,$http,$state,Shop,AuthService,WC,userBasicInfo, Flags){
  console.log("shippingAddress ctrl is working");
  var shippingAddress=this;
  shippingAddress.frm={}

  if(userBasicInfo.ship){
    $scope.ship=userBasicInfo.ship;
    userBasicInfo.billing =$scope.billing;
  }
  else {
    $scope.ship = {
      first_name: '',
      last_name: '',
      country: '',
      phone: '',
      email: '',
      address_1: '',
      address_2: '',
      city: '',
      state: '',
      postcode: ''
    };

    $scope.billing = {
      phone: '',
      email: ''
    };

    $scope.tmp = {
      note: '',
      country: ''
    };

    $scope.user = {
      id: AuthService.id(),
      username: AuthService.username(),
      email: AuthService.email(),
      name: AuthService.name(),
      isLogin: AuthService.isAuthenticated()
    };


    if ($scope.user.isLogin) {
      $ionicLoading.show();
      WC.api().get('customers/' + $scope.user.id, function (err, data, res) {
        if (err) console.log(err);
        var user = JSON.parse(res).customer;
        console.log(user);
        $scope.billing = {
          first_name: (user.billing_address.first_name ? user.billing_address.first_name : ''),
          last_name: (user.billing_address.last_name ? user.billing_address.last_name : ''),
          email: $scope.user.email,
          phone: (user.billing_address.phone ? user.billing_address.phone : ''),
          country: (user.billing_address.country ? user.billing_address.country : ''),
          address_1: (user.billing_address.address_1 ? user.billing_address.address_1 : ''),
          address_2: (user.billing_address.address_2 ? user.billing_address.address_2 : ''),
          city: (user.billing_address.city ? user.billing_address.city : ''),
          state: (user.billing_address.state ? user.billing_address.state : ''),
          postcode: (user.billing_address.postcode ? user.billing_address.postcode : '')
        };
        $scope.ship = {
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email,
          phone: (user.shipping_address.phone ? user.shipping_address.phone : ''),
          country: (user.shipping_address.country ? user.shipping_address.country : ''),
          address_1: (user.shipping_address.address_1 ? user.shipping_address.address_1 : ''),
          address_2: (user.shipping_address.address_2 ? user.shipping_address.address_2 : ''),
          city: (user.shipping_address.city ? user.shipping_address.city : ''),
          state: (user.shipping_address.state ? user.shipping_address.state : ''),
          postcode: (user.shipping_address.postcode ? user.shipping_address.postcode : '')
        };
        userBasicInfo.ship = $scope.ship;                               //save ship details in service
        userBasicInfo.billing =$scope.billing;                         //save billing details in service

        if ($scope.ship.country) {
          $scope.tmp.country = 'loading country ...'
          $http.get("https://api.theprintful.com/countries").success(function (x) {
            var tmp = x.result;
            for (var i in tmp) {
              if (tmp[i].code == $scope.ship.country)
                $scope.tmp.country = tmp[i].name
            }
            $ionicLoading.hide();
          }).error(function (err) {
            $scope.tmp.country = '';
          });
        }
        $scope.ship.postcode = parseInt($scope.ship.postcode);
        $scope.billing.phone = parseInt($scope.billing.phone);
        console.log($scope.ship);
        $ionicLoading.hide();
      })
    }

    $scope.shipping_lines = {
      method_id: '',
      method_title: '',
      total: 0
    };

  }
  $scope.shipping = Shop.shipping;
  $scope.payment = Shop.payment;

  $scope.setShip = function(x){
    $scope.shipping_lines = [{
      method_id: x.id,
      method_title: x.name,
      total: x.cost
    }];
  }

  $scope.setPayment = function(x){
    $scope.payment_details = {
      method_id: x.id,
      method_title: x.name,
      paid: false
    };
  }

  $scope.closeModal = function(){
    $scope.modal.hide();
  }
  $scope.doConfirm = function(x){
    userBasicInfo.ship=$scope.ship;         //save ship details in service
    userBasicInfo.billing=$scope.billing;         //save ship details in service
    var order = {
      payment_details  : $scope.payment_details,
      shipping_address : $scope.ship,
      billing_address  : $scope.billing,
      shipping_lines   : $scope.shipping_lines,
      customer_id      : $scope.user.id,
      //note             : $scope.tmp.note
    }
    console.log(order);
    window.localStorage.setItem(Shop.name+"-order", JSON.stringify(order));
    $state.go('confirm');
  }

  $scope.showCountry = function(){
    $ionicLoading.show();
    $http.get("https://api.theprintful.com/countries").success(function(x){
      $scope.countries = x.result;
      $ionicLoading.hide();
    }).error(function(err){
      $ionicLoading.hide();
      $scope.showError("Error in connection. Please try again.");
    });

    $ionicModal.fromTemplateUrl('templates/countryList-Modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };
  $scope.setCountry = function(x){
    console.log(x);
    $scope.ship.country=x.name;
    $scope.closeModal();
  }

  $rootScope.closeModal = function() {
    $scope.modal.hide();
    console.log("close modal");
  };
  //shippingAddress.submitDetails = function(){
  //
  //  console.log(shippingAddress.userDetail);
  //  $state.go('confirm');
  //}
  shippingAddress.goBack  = function(){
    $state.go('cart')
  }
  Flags.redirectFormCart=0;
  }
