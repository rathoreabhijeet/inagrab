angular.module('starter')
  .controller('allProductsCtrl', allProductsCtrl);

function allProductsCtrl($state, $scope, $rootScope, WC, $ionicLoading, paramService, $timeout,
  $ionicScrollDelegate, AuthService, $window, Shop, wooData, $http,
  $ionicSideMenuDelegate, customBackend, FilteredData, convertCurrency) {
  console.log("allProduct ctrl is working");
  var allProducts = this;

  var devH = $window.innerHeight;
  var devW = $window.innerWidth;
  allProducts.productImage = { 'min-height': 0.33 * devW - 10 + 'px' };
  allProducts.symbol = paramService.selectedCurrency.symbol
  $rootScope.moreDataAvailable = true;
  allProducts.searchResult = [];
  allProducts.firstLoad = true;
  if (FilteredData.options.q) {
    allProducts.searchText = FilteredData.options.q;
  }

  //**************************************************************************************************//

  // var cart = window.localStorage.getItem(Shop.name + "-cart");
  // cart = JSON.parse(cart);

  // if (cart && cart.length > 0) {
  //   $scope.totalCartItem = 0;
  //   for (var i in cart)
  //     $scope.totalCartItem += cart[i].qty;
  // }

  // allProducts.category = _.find(wooData.categoryInfo, function (n) {
  //   return n.name == paramService.categoryName;
  // });


  allProducts.searchCategory = paramService.categoryName;
  allProducts.category = {};
  allProducts.category.name = paramService.categoryTitle;
  allProducts.categoryImage = paramService.categoryImage;
  // $rootScope.selectedCategory = angular.copy(paramService.categoryName);
  // allProducts.searchCategoryIndex = _.findIndex(wooData.categoryInfo, {name: allProducts.searchCategory});
  // console.log(allProducts.searchCategoryIndex);

  // $scope.more = false;
  var resultsPage = 0;


  function ObjecttoParams(obj) {
    var p = [];
    for (var key in obj) {
      p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
  };


  function getWebsiteDetailsOfProduct(productList, index) {
    if (index < productList.length) {
      $http.get('http://inagrab.com/api/papi.php?pid=' + productList[index].id).then(function (success) {
        if (success.data && success.data.data && success.data.data.length) {
          productList[index].websiteArray = success.data.data;
        }
        index = index + 1;
        getWebsiteDetailsOfProduct(productList, index);
      }, function (error) {
        console.log(error);
      })
    } else {
      $ionicLoading.hide();
    }
  }

  allProducts.loadMore = function () {
    if (FilteredData.savedData.length) {
      $timeout(function () {
        allProducts.searchResult = FilteredData.savedData;
        FilteredData.savedData = [];
        console.log(allProducts.searchResult);
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }, 200)
    }
    else {
      if ($rootScope.moreDataAvailable) {
        if (!allProducts.firstLoad) {
          $rootScope.$broadcast('infiniteScrollCalled');
        }
        allProducts.firstLoad = false;

        $timeout(function () {
          $rootScope.$broadcast('searchWithFilters');
        }, 200)
      }
    }
  }

  //apply filter on products
  $rootScope.applyFilter = function (rating, review, price, website) {
    console.log(website);
    $rootScope.rating = parseInt(rating);
    $rootScope.review = parseInt(review);
    $rootScope.price = parseInt(price);
    $rootScope.website = website;
    $ionicSideMenuDelegate.toggleLeft();
  }

  //rating the app
  $scope.rating = function (averageRating, val, caseval) {
    averageRating = parseInt(averageRating);
    switch (caseval) {
      case 1:
        if (averageRating >= val) {
          return true;
        }
        break;
      case 2:
        if (averageRating < val) {
          return true;
        }
        break;
      case 3:
        if (averageRating > val) {
          return true;
        }
        break;
      case 4:
        if (averageRating <= val) {
          return true;
        }
        break;
    }

  }

  allProducts.GoToUserProfile = function () {

    console.log(AuthService.isAuthenticated());
    var user = window.localStorage.getItem(Shop.name + "-user");
    if (AuthService.isAuthenticated() && user) {
      $state.go('userProfile');
    }
    else {
      if (window.cordova) {
        var onConfirm = function (buttonIndex) {

          if (buttonIndex == 1) {
            paramService.signin = {};
            paramService.signin.prevState = 'category';
            $state.go('signIn');
          }
          else {
            console.log('dismissed');
          }
        };
        navigator.notification.confirm(
          'Only logged in users can see their profile', // message
          onConfirm,            // callback to invoke with index of button pressed
          'Not logged in !',           // title
          ['Login', 'Dismiss']     // buttonLabels
        );
      }
      else {
        var r = confirm('Only logged in users can see their profile. Want to login?');
        if (r == true) {
          paramService.signin = {};
          paramService.signin.prevState = 'category';
          $state.go('signIn');

        } else {
          console.log('dismissed');
        }
      }
    }

  };


  allProducts.GoToCart = function () {
    $state.go('cart', { prevState: 'app.allProducts' });
  }

  allProducts.GoBack = function () {
    FilteredData.options = {};
    paramService.productDetail = {};
    paramService.categoryName = null;
    $state.go('category');
  }

  allProducts.GoToProductDettails = function (product) {
    console.log(product);
    // $ionicLoading.show({ template: 'Loading.' });
    // WC.api().get('products/' + product.ID, function (err, data, res) {
    //   if (err) {
    //     console.log(err);
    //     $ionicLoading.hide();
    //   } else {
    //     if (JSON.parse(res).errors) {
    //       console.log(JSON.parse(res).errors[0].message);
    //       $ionicLoading.hide();
    //     } else {
    //       console.log(JSON.parse(res).product);
    paramService.productDetail = product;
    paramService.productDetail.fromSearch = 'app.allProducts';
    _.each(allProducts.searchResult, function (item) {
      FilteredData.savedData.push(item);
    })
    // FilteredData.savedData = allProducts.searchResult;
    $state.go('productDetail');
    $ionicLoading.hide();
  // }
  //   }
  // });
}

allProducts.itemAdded = function (x, qty) {
  var cart = window.localStorage.getItem(Shop.name + "-cart");
  if (cart) {
    var exist = false;
    cart = JSON.parse(cart);
    for (i in cart) {
      if (cart[i].id == x.id) {
        exist = true;
        cart[i].qty = cart[i].qty + qty;
        window.localStorage.setItem(Shop.name + "-cart", JSON.stringify(cart));
        $scope.showSuccess('Product added successfully');
        $scope.updateCart();
        break;
      }
    }
    if (!exist) {
      if (x.option)
        cart.push({
          id: x.id,
          title: x.title,
          price: x.price,
          img: x.featured_src,
          qty: 1,
          variations: { name: x.name, option: x.option }
        });
      else
        cart.push({ id: x.id, title: x.title, price: x.price, img: x.featured_src, qty: 1 });
      window.localStorage.setItem(Shop.name + "-cart", JSON.stringify(cart));
      $scope.showSuccess('Product added successfully');
    }
  } else {
    var tmp = [];
    if (x.option)
      tmp.push({
        id: x.id,
        title: x.title,
        price: x.price,
        img: x.featured_src,
        qty: 1,
        variations: { name: x.name, option: x.option }
      });
    else
      tmp.push({ id: x.id, title: x.title, price: x.price, img: x.featured_src, qty: 1 });
    window.localStorage.setItem(Shop.name + "-cart", JSON.stringify(tmp));
    $scope.showSuccess('Product added successfully');
  }
  console.log(cart);
  $scope.updateCart();

};

$scope.updateCart = function () {
  var cart = window.localStorage.getItem(Shop.name + "-cart") ? JSON.parse(window.localStorage.getItem(Shop.name + "-cart")) : '';
  if (cart.length > 0) {
    $scope.totalCartItem = 0;
    for (var i in cart)
      $scope.totalCartItem += cart[i].qty;
  }
  else $scope.totalCartItem = 0;
};

$scope.showSuccess = function (x) {

  $ionicLoading.show({
    template: '<div class="info"><i class="icon ok ion-ios-checkmark text-xxx-large"></i></div><div>' + x + '</div>'
  });
  $timeout(function () {
    $ionicLoading.hide();
  }, 1000);
};
$scope.$on('filterApplied', function () {
  allProducts.searchResult = FilteredData.data;
  allProducts.category.name = paramService.categoryTitle;
  allProducts.categoryImage = paramService.categoryImage;
  $timeout(function () {
    $ionicScrollDelegate.resize();
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }, 200)
})

allProducts.cancelSearchByText = function () {
  allProducts.searchText = '';
  FilteredData.options.q = '';
  $rootScope.$broadcast('textSearchCancelled');
}

}
