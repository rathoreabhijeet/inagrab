angular.module('starter')
  .controller('productDetailCtrl', productDetailCtrl);

function productDetailCtrl(AuthService, $state, $scope, $ionicLoading, $timeout, Shop, paramService,
  $cordovaToast, $http, $cordovaInAppBrowser, $rootScope, $ionicModal, $window, customBackend,
  $ionicSlideBoxDelegate, $ionicScrollDelegate, $location, convertCurrency, $sce, CategoryData, AllProductsResults,
  imageBaseUrl, productUrlRedirection) {
  console.log("productDetail ctrl is working");
  var productDetail = this;
  productDetail.comparisonData = [];
  productDetail.imagesData = [];
  productDetail.booksData = [];
  productDetail.socialData = [];
  productDetail.videosData = [];
  productDetail.reviewsData = [];
  productDetail.offersData = [];
  productDetail.selectedTab = 0;
  productDetail.swiper = {};
  console.log(paramService.productDetail);
  var devH = $window.innerHeight;
  var devW = $window.innerWidth;
  productDetail.fullDim = { 'height': devH - 88 + 'px', 'width': devW + 'px' };
  productDetail.symbol = paramService.selectedCurrency.symbol

  productDetail.selectedProduct = paramService.productDetail;


  var cart = window.localStorage.getItem(Shop.name + "-cart");
  cart = JSON.parse(cart);

  if (cart && cart.length > 0) {
    $scope.totalCartItem = 0;
    for (var i in cart)
      $scope.totalCartItem += cart[i].qty;
  }

  function convertToUsd(price, currency) {
    if (currency == 'USD') {
      return parseFloat(parseFloat(price).toFixed(2));
    }
    else {
      var convertedPrice = (price / $rootScope.currencyConversionData[currency]).toFixed(2);
      return parseFloat((price / $rootScope.currencyConversionData[currency]).toFixed(2));
    }
  }

  // productDetail.selectTab = function (tab) {
  //   productDetail.selectedTab = tab;
  // }

  productDetail.slideHasChanged = function (index) {
    productDetail.selectedTab = index;
    productDetail.anchorScroll(index);
  }

  productDetail.onReadySwiper = function (swiper) {
    swiper.on('slideChangeEnd', function () {
      $timeout(function () {
        productDetail.selectedTab = productDetail.swiper.activeIndex;
        productDetail.anchorScroll(productDetail.selectedTab);
      }, 100)
    })
  }

  productDetail.seeReviews = function () {
    $state.go('reviews', { link: productDetail.iframeLink });
  }

  productDetail.anchorScroll = function (index) {
    // $ionicScrollDelegate.$getByHandle('tabs').scrollBy(20,0,true);
    switch (index) {
      case 0:
        $location.hash('slide0');
        break;
      case 1:
        $location.hash('slide1');
        break;
      case 2:
        $location.hash('slide2');
        break;
      case 3:
        $location.hash('slide3');
        break;
      case 4:
        $location.hash('slide4');
        break;
      case 5:
        $location.hash('slide5');
        break;
      case 6:
        $location.hash('slide6');
        break;
      case 7:
        $location.hash('slide7');
        break;
    }
    $ionicScrollDelegate.$getByHandle('slider').anchorScroll(true);
  }

  productDetail.goToSlide = function (index) {
    productDetail.selectedTab = index;
    productDetail.swiper.slideTo(index, 400);
    productDetail.anchorScroll(index);
    console.log(productDetail.selectedTab);
    // productDetail.clicked = true;
    // $timeout(function(){
    //     productDetail.clicked = false;
    // },500)
  }

  function ObjecttoParams(obj) {
    var p = [];
    for (var key in obj) {
      p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    console.log(p.join('&'));
    return p.join('&');
  };

  function extractIframe(text) {
    productDetail.iframeLink = text.substring((text.indexOf('="')) + 2, text.indexOf('"', (text.indexOf('="')) + 2));
    console.log(productDetail.iframeLink);
    // productDetail.iframeText = text;
  }

  productDetail.goToCategory = function () {
    var category = _.find(CategoryData.categories, function (cat) {
      return cat.term_taxonomy_id == productDetail.category.term_taxonomy_id
    })
    paramService.categoryName = category.term_taxonomy_id;
    paramService.categoryImage = category.img;
    paramService.categoryTitle = category.name;
    console.log(AllProductsResults.data);
    $state.go('app.allProducts');
  }

  function loadAdditionalDetails() {
    $ionicLoading.show({ template: 'Loading..' });
    $http({
      method: "POST",
      url: customBackend + '/additionalinfo',
      data: ObjecttoParams({
        pid: paramService.productDetail.ID
      }),
      headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' }
    }).then(function (success) {
      console.log(success);
      $ionicLoading.hide();
      productDetail.additional = [];
      productDetail.additional = success.data.data;
      if (success.data.category && success.data.category.length) {
        productDetail.category = success.data.category[0];
      }
      if (success.data.subcategory && success.data.subcategory.length) {
        productDetail.subcategory = success.data.subcategory[0];
      }
      if (success.data.brand && success.data.brand.length) {
        productDetail.brand = success.data.brand[0];
      }
      if (success.data.reviews && success.data.reviews.length) {
        extractIframe(success.data.reviews[0].content);
      }
      if (success.data.images && success.data.images.length) {
        productDetail.featureImage = imageBaseUrl+success.data.images[0].img;
        _.each(success.data.images, function (image) {
          image.img = imageBaseUrl+image.img;
          productDetail.imagesData.push(image);
        })
      }
    })
  }

  function onLoad() {
    $ionicLoading.show({ template: 'Loading..' });
    $http({
      method: "POST",
      url: customBackend + '/compareproducts',
      data: ObjecttoParams({
        pid: paramService.productDetail.ID
      }),
      headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' }
    }).then(function (success) {
      console.log(success);
      if (success.data && success.data.products && success.data.products.length) {
        _.each(success.data.products, function (item) {
          if (item.type == 'product') {
            if (item.currencyCode && (item.price || item.price==0 || item.price=='0')) {
              item.price = convertCurrency.convert(item.price, item.currencyCode);
              item.currency = '$';
              productDetail.comparisonData.push(item);
            }

            if (item.reviews && item.reviews.length) {
              productDetail.reviewsData.push(item);
            }

            if (!item.domain) {
              productDetail.offersData.push(item);
            }
          }
          else if (item.type == 'videos') {
            productDetail.videosData.push(item);
          }
          else if (item.type == 'social') {
            productDetail.socialData.push(item);
          }
          else if (item.type == 'books') {
            productDetail.booksData.push(item);
          }
          else if (item.type == 'news' && item.description == '') {
            productDetail.imagesData.push(item);
          }
          else if (item.type == 'news' && item.description != '') {
            productDetail.offersData.push(item);
          }
        })
        productDetail.showSlidercontent = true;
        console.log('comparison', productDetail.comparisonData);
        console.log('social', productDetail.socialData);
        console.log('videos', productDetail.videosData);
        console.log('books', productDetail.booksData);
        console.log('images', productDetail.imagesData);
        console.log('offers', productDetail.offersData);
        findMinimumAffiliate();
        $timeout(function () { $ionicSlideBoxDelegate.update() }, 200);
      } else if (success.data && success.data.products && success.data.products.length == 0) {
        setDefaultPrice();
        productDetail.showNoData = true;
      }
      else {
        productDetail.showNoData = true;
      }
      loadAdditionalDetails();

      $ionicLoading.hide();
    }, function (error) {
      console.log(error);
      $ionicLoading.hide();
    })
  }

  function setDefaultPrice() {
    productDetail.minAffiliatePrice = productDetail.selectedProduct.actualMinPrice;
    productDetail.minAffiliate = 'amazon.com';
    productDetail.minAffiliateUrl = productUrlRedirection + productDetail.selectedProduct.product_url;
  }

  function findMinimumAffiliate() {
    var prices = productDetail.comparisonData.map(function (affiliate) {
      return parseFloat(affiliate.price);
    });
    console.log(prices);
    var minPrice = _.min(prices);
    console.log(minPrice);

    var minIndex = _.indexOf(prices, minPrice);
    if (minIndex != -1) {
      productDetail.minAffiliate = productDetail.comparisonData[minIndex].domain;
      productDetail.minAffiliateUrl = productDetail.comparisonData[minIndex].url;
      productDetail.minAffiliatePrice = productDetail.comparisonData[minIndex].price
      console.log(productDetail.comparisonData);
    }

  }

  onLoad();


  productDetail.goToUrl = function (url) {
    console.log(url);
    var options = {
      location: 'no',
      clearcache: 'yes',
      toolbar: 'yes'
    };

    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open(url, '_blank', options)
        .then(function (event) {
          console.log(event);
          // success
        })
        .catch(function (event) {
          console.log(event);
          // error
        });


      //$cordovaInAppBrowser.close();

    }, false);
    if (!window.cordova) {
      window.open(url);
    }
  }


  productDetail.itemAdded = function (x, qty) {
    var cart = window.localStorage.getItem(Shop.name + "-cart");
    if (cart) {
      var exist = false;
      cart = JSON.parse(cart);
      for (i in cart) {
        if (cart[i].id == x.id) {
          exist = true;
          cart[i].qty = cart[i].qty + qty;
          window.localStorage.setItem(Shop.name + "-cart", JSON.stringify(cart));
          $scope.showSuccess('Product added successfully');
          $scope.updateCart();
          break;
        }
      }
      if (!exist) {
        if (x.option)
          cart.push({ id: x.id, title: x.title, price: x.price, img: x.featured_src, qty: 1, variations: { name: x.name, option: x.option } });
        else
          cart.push({ id: x.id, title: x.title, price: x.price, img: x.featured_src, qty: 1 });
        window.localStorage.setItem(Shop.name + "-cart", JSON.stringify(cart));
        $scope.showSuccess('Product added successfully');
      }
    } else {
      var tmp = [];
      if (x.option)
        tmp.push({ id: x.id, title: x.title, price: x.price, img: x.featured_src, qty: 1, variations: { name: x.name, option: x.option } });
      else
        tmp.push({ id: x.id, title: x.title, price: x.price, img: x.featured_src, qty: 1 });
      window.localStorage.setItem(Shop.name + "-cart", JSON.stringify(tmp));
      $scope.showSuccess('Product added successfully');
    }
    console.log(cart);
    $scope.updateCart();

  };
  $scope.updateCart = function () {
    var cart = window.localStorage.getItem(Shop.name + "-cart") ? JSON.parse(window.localStorage.getItem(Shop.name + "-cart")) : '';
    if (cart.length > 0) {
      $scope.totalCartItem = 0;
      for (var i in cart)
        $scope.totalCartItem += cart[i].qty;
    }
    else $scope.totalCartItem = 0;
  };

  $scope.showSuccess = function (x) {

    $ionicLoading.show({
      template: '<div class="info"><i class="icon ok ion-ios-checkmark text-xxx-large"></i></div><div>' + x + '</div>'
    });
    $timeout(function () {
      $ionicLoading.hide();
    }, 1000);
  };

  productDetail.GoToCart = function () {
    $state.go('cart', { prevState: 'productDetail' });
  };

  productDetail.GoBack = function () {
    console.log(AllProductsResults.data);
    $state.go(paramService.productDetail.fromSearch);
  };
  productDetail.GoToUserProfile = function () {

    console.log(AuthService.isAuthenticated());
    var user = window.localStorage.getItem(Shop.name + "-user");
    if (AuthService.isAuthenticated() && user) {
      $state.go('userProfile');
    }
    else {
      if (window.cordova) {
        var onConfirm = function (buttonIndex) {

          if (buttonIndex == 1) {
            paramService.signin = {};
            paramService.signin.prevState = 'category';
            $state.go('signIn');
          }
          else {
            console.log('dismissed');
          }
        };
        navigator.notification.confirm(
          'Only logged in users can see their profile', // message
          onConfirm,            // callback to invoke with index of button pressed
          'Not logged in !',           // title
          ['Login', 'Dismiss']     // buttonLabels
        );
      }
      else {
        var r = confirm('Only logged in users can see their profile. Want to login?');
        if (r == true) {
          paramService.signin = {};
          paramService.signin.prevState = 'category';
          $state.go('signIn');

        } else {
          console.log('dismissed');
        }
      }
    }

  };

  //rating the app
  $scope.rating = function (averageRating, val, caseval) {
    averageRating = parseInt(averageRating);
    switch (caseval) {
      case 1:
        if (averageRating >= val) {
          return true;
        }
        break;
      case 2:
        if (averageRating < val) {
          return true;
        }
        break;
      case 3:
        if (averageRating > val) {
          return true;
        }
        break;
      case 4:
        if (averageRating <= val) {
          return true;
        }
        break;
    }

  }

  productDetail.openLightbox = function () {
    $ionicModal.fromTemplateUrl('templates/lightbox.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      productDetail.modal = modal;
      productDetail.modal.show();
    });
  }

  productDetail.toggleReviewCollapse = function (index) {
    if (productDetail.expandDomain == index) {
      productDetail.expandDomain = -1;
    }
    else {
      productDetail.expandDomain = index;
    }
  }

  productDetail.hideLightbox = function () {
    productDetail.modal.hide();
    productDetail.modal.remove();
  }
}
