angular.module('starter')
  .controller('categoryCtrl', categoryCtrl);

function categoryCtrl($window, $rootScope, backend, $timeout, $scope, $state, $ionicLoading,
  Shop, WC, AuthService, paramService, wooData, $cordovaToast, $http, bestDealsData, customBackend,
  CategoryData, convertCurrency, BestDealsResults) {

  var category = this;
  var deviceWidtht = $window.innerWidth;
  var deviceHeight = $window.innerHeight;
  //console.log("remining height" + (deviceHeight - 104) / 4);
  //console.log("dev width" + deviceWidtht);
  category.margin5percent = { 'margin-top': (deviceHeight / 100) * 3 + '%' };
  category.imgheight = { 'height': (deviceHeight - 84) / 4 + 'px' };
  category.content = { 'height': (deviceHeight - 84) + 'px' };
  category.symbol = paramService.selectedCurrency.symbol;
  category.categoryArray = CategoryData.categories;
  console.log(category.categoryArray);
  //********************************************************************************************************//

  //console.log(AuthService.isAuthenticated());

  //console.log("category ctrl is working");
  //console.log(AuthService);

  $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
    viewData.enableBack = false;
  });

  // var cart = window.localStorage.getItem(Shop.name + "-cart");
  // cart = JSON.parse(cart);

  // if (cart && cart.length > 0) {
  //   $scope.totalCartItem = 0;
  //   for (var i in cart)
  //     $scope.totalCartItem += cart[i].qty;
  // }
  // category.categoryArray = wooData.categoryInfo;

  // if (!_.isEmpty(wooData.categoryInfo)) {
  //   //console.log(wooData.categoryInfo);
  //   // $rootScope.categoryArray = wooData.categoryInfo;
  // }
  // else {
  //   $ionicLoading.show();
  //   WC.api().get('products/categories', function (err, data, res) {
  //     if (res) {
  //       var array = JSON.parse(res).product_categories;
  //       console.log(array);
  //       _.each(array, function (n) {
  //         category.categoryArray.push(n);
  //       })
  //       // console.log(category.categoryArray);
  //       // console.log(CategoryData.categories);
  //       _.each(CategoryData.categories, function (cat, catIndex) {
  //         if (catIndex == 0) {
  //           cat.image = '';
  //         }
  //         else {
  //           var index = _.findIndex(category.categoryArray, function (category) {
  //             // console.log(category.id);
  //             // console.log(cat.term_taxonomy_id);
  //             return category.id == cat.term_taxonomy_id;
  //           })
  //           // console.log(index);
  //           cat.image = category.categoryArray[index].image;
  //         }
  //       })
  //       category.categoryArray = _.reject(category.categoryArray, function (cat) {
  //         return cat.parent != 0;
  //       })
  //       // console.log(CategoryData.categories);
  //       // $rootScope.categoryArray = category.categoryArray;
  //       $ionicLoading.hide();
  //     }
  //   })
  // }

  function ObjecttoParams(obj) {
    var p = [];
    for (var key in obj) {
      p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
  };

  category.findProduct = function () {
    var searchPage = 1;
    category.noSearchResult = false;
    category.searchResult = [];
    if (category.searchText !== '') {
      $ionicLoading.show({ template: 'Searching..' });
      $http({
        method: "POST",
        url: customBackend + '/search',
        data: ObjecttoParams({
          q: category.searchText,
          count: 10,
          skip: 1
        }),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).then(function (success) {
        // console.log(success);
        if (success.data.products.length) {
          $timeout(function () {
            category.searchResult = success.data.products;

            _.each(category.searchResult, function (product) {
              category.affiliateSpecificPrices = [];
              if (product.aff_data && product.aff_data.length) {
                _.each(product.aff_data, function (aff) {
                  if (aff.type == 'product' && aff.currencyCode && (aff.price || aff.price == 0 || aff.price == '0')) {
                    category.affiliateSpecificPrices.push(convertCurrency.convert(aff.price, aff.currencyCode));
                  }
                })
                if (category.affiliateSpecificPrices.length) {
                  product.actualMinPrice = _.min(category.affiliateSpecificPrices);
                }
                else if (product.price) {
                  product.actualMinPrice = convertCurrency.convert(product.price, 'USD');
                }
                else {
                  product.invalid = true;
                }
              }
              else if (product.price) {
                product.actualMinPrice = convertCurrency.convert(product.price, 'USD');
              }
              else {
                product.invalid = true;
              }
            })


            if (category.searchResult.length) {
              category.searchResults = true;
            }
            else {
              category.noSearchResult = true;
              if (window.cordova)
                $cordovaToast.showShortBottom('No products found. Please modify search');
            }
          }, 20);
        } else {
          category.noSearchResult = true;
          if (window.cordova)
            $cordovaToast.showShortBottom('No products found. Please modify search');
        }

        $ionicLoading.hide();
      }, function (error) {
        //console.log(error);
        if (window.cordova)
          $cordovaToast.showShortBottom('No products found. Please modify search');
        $ionicLoading.hide();
      })
    }

  };


  category.GoToUserProfile = function () {
    //console.log(AuthService.isAuthenticated());
    var user = window.localStorage.getItem(Shop.name + "-user");
    if (AuthService.isAuthenticated() && user) {
      $state.go('userProfile');
    }
    else {
      if (window.cordova) {
        var onConfirm = function (buttonIndex) {

          if (buttonIndex == 1) {
            paramService.signin = {};
            paramService.signin.prevState = 'category';
            $state.go('signIn');
          }
          else {
            //console.log('dismissed');
          }
        };
        navigator.notification.confirm(
          'Only logged in users can see their profile', // message
          onConfirm,            // callback to invoke with index of button pressed
          'Not logged in !',           // title
          ['Login', 'Dismiss']     // buttonLabels
        );
      }
      else {
        var r = confirm('Only logged in users can see their profile. Want to login?');
        if (r == true) {
          paramService.signin = {};
          paramService.signin.prevState = 'category';
          $state.go('signIn');

        } else {
          //console.log('dismissed');
        }
      }
    }

  };



  category.GoToAllProducts = function (cat) {
    console.log(cat);
    paramService.categoryName = cat.term_taxonomy_id;
    paramService.categoryImage = cat.img;
    paramService.categoryTitle = cat.name;
    console.log(paramService);
    $state.go('app.allProducts');
  }
  category.GoToRandomProducts = function () {
    $state.go('randomProducts');
  }
  category.GoToCart = function () {
    $state.go('cart', { prevState: 'category' })
  }
  category.GoToHome = function () {
    $state.go('searchProduct');
  }

  category.productDesc = function (selectedItem) {
    console.log("selectedItem", selectedItem);
    $ionicLoading.show({ template: 'Fetching product details ...' });
    // WC.api().get('products/' + selectedItem.ID, function (err, data, res) {
    //   // console.log(data);
    //   // console.log(res);
    //   if (err) {
    //     console.log(err);
    //     $ionicLoading.hide();
    //   } else {
    //     if (JSON.parse(res).errors) {
    //       //console.log(JSON.parse(res).errors[0].message);
    //       $ionicLoading.hide();
    //     } else {
    //       //console.log(JSON.parse(res).product);
    paramService.productDetail = selectedItem;
    paramService.productDetail.fromSearch = 'category';
    $state.go('productDetail');
    $ionicLoading.hide();
    //     }
    //   }
    // });
  }

  category.bestDeals = function () {
    if (category.searchText) {
      paramService.bestDeal = {};
      paramService.bestDeal.bestDealParam1 = category.searchText;
      paramService.bestDeal.bestDealParam2 = null;
      paramService.bestDeal.bestDealParam3 = 'category';
      BestDealsResults.data = category.searchResult;
      $state.go('bestDeals');
    }
    else {
      if (window.cordova) {
        if (!category.searchText) {
          $cordovaToast.showShortBottom('Please enter a search term first');
        }
        else if (!category.searchResult.length) {
          $cordovaToast.showShortBottom('No search results found. Please modify your search');
        }
      }
      else {
        if (!category.searchText) {
          alert('Please enter a search term first');
        }
        else if (!category.searchResult.length) {
          alert('No search results found. Please modify your search');
        }
      }
    }
  }

  category.cancelSearch = function () {
    category.searchText = '';
    category.searchResult.length = 0;
    category.searchResults = false;
  }



}
