angular.module('starter')
  .controller('inviteCtrl', inviteCtrl);

function inviteCtrl($window, $state, $cordovaToast) {
  var invite = this;

  console.log("inviteCtrl ctrl is working");
  var deviceWidtht = $window.innerWidth;
  var deviceHeight = $window.innerHeight;
  invite.main = { 'height': 0.7 * deviceHeight + 'px' };
  invite.center = { 'margin-top': .5 * (deviceHeight - 345) - 50 + 'px' };

  invite.facebookInvite = function () {
    window.plugins.socialsharing.shareViaFacebook('Optional message, may be ignored by Facebook app',
      null,
      'http://inagrab.com',
      function () {
        console.log('share ok');
        $cordovaToast.showShortBottom('Invite Shared');
      },
      function (errormsg) {
        console.log(errormsg)
        $cordovaToast.showShortBottom('Error sending invite');
      });
  };

  //google plus invite function
  invite.whatsappInvite = function () {
    window.plugins.socialsharing.shareViaWhatsApp('inagrab - we cut the crap ! Best deal platform!',
      null /* img */,
      'http://inagrab.com' /* url */,
      function () {
        console.log('share ok');
        $cordovaToast.showShortBottom('Invite Shared');
      },
      function (errormsg) {
        $cordovaToast.showShortBottom('Error sending invite');
      })
  }

  //phone Contact invite function
  invite.phoneContactInvite = function () {
    console.log('phoneContactInvite work')
    window.plugins.socialsharing.shareViaSMS('inagrab - we cut the crap ! Best deal platform! - http://inagrab.com', null, function (msg) {
      console.log('ok: ' + msg)
    },
      function (msg) {
        $cordovaToast.showShortBottom('Error sending invite');
      })
  }

  //Email Contact invite function
  invite.emailContactInvite = function () {
    window.plugins.socialsharing.shareViaEmail(
      'inagrab - we cut the crap ! Best deal platform! - http://inagrab.com', // can contain HTML tags, but support on Android is rather limited:  http://stackoverflow.com/questions/15136480/how-to-send-html-content-with-image-through-android-default-email-client
      'inagrab - we cut the crap',
      null, // TO: must be null or an array
      null, // CC: must be null or an array
      null, // BCC: must be null or an array
      null, // FILES: can be null, a string, or an array
      function () {
        console.log('success email contact sharing');
        $cordovaToast.showShortBottom('Invite Shared');
      }, // called when sharing worked, but also when the user cancelled sharing via email. On iOS, the callbacks' boolean result parameter is true when sharing worked, false if cancelled. On Android, this parameter is always true so it can't be used). See section "Notes about the successCallback" below.
      function (err) {
        console.log('email contact sharing err:' + err)
        $cordovaToast.showShortBottom('Error sending invite');
      } // called when sh*t hits the fan
    );
  }

  invite.GoBack = function () {
    $state.go('userProfile');
  }

}
