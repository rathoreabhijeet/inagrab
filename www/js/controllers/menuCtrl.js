angular.module('starter.controllers', [])
  .controller('menuCtrl', menuCtrl)
function menuCtrl($window, $ionicModal, $scope, $rootScope, $state, AuthService, Shop, wooData, $ionicLoading, WC,
  CategoryData, AffiliateData, customBackend, $http, FilteredData, paramService, convertCurrency, $timeout, $cordovaToast) {
  var menu = this;
  //variables for StarRating

  var devWidth = $window.innerWidth;
  var deviceHeight = $window.innerHeight;
  menu.fullHeight = { 'height': 1.2 * deviceHeight + 'px' };
  menu.allBrands = CategoryData.brands;
  menu.allCategories = CategoryData.categories;
  menu.allSubCategories = CategoryData.subcategories;

  menu.allWebsites = AffiliateData.data;
  menu.selectedBrand = 'All';
  menu.selectedBrandId = 0;
  FilteredData.data.length = 0;
  menu.selectedWebsite = 'All';
  menu.selectedCat = 'All';
  console.log(CategoryData.categories);
  // function getCurrentCategory() {
  menu.selectedCatId = paramService.categoryName;
  menu.selectedCat = paramService.categoryTitle;

  //   console.log(paramService);

  // }
  // getCurrentCategory();

  menu.selectedSortId = 1;
  menu.selectedSubCat = 'All';
  menu.selectedSubCatId = 0;
  menu.allSorts = [
    { 'name': 'Order by Title (a-z)', 'id': 1 },
    { 'name': 'Order by Title (z-a)', 'id': 2 },
    { 'name': 'Order by Price (min. to max.)', 'id': 3 },
    { 'name': 'Order by Price (max. to min.)', 'id': 4 },
    { 'name': 'Newest first', 'id': 1 }

  ];
  menu.selectedSort = 'Order by Title (a-z)';
  menu.priceMin = paramService.priceFilter.min;
  menu.priceMax = paramService.priceFilter.max;
  menu.minPriceSelected = paramService.priceFilter.min;
  menu.maxPriceSelected = paramService.priceFilter.max;
  menu.symbol = paramService.selectedCurrency.symbol;
  //--------------------------------------------------------------------------------------//
  menu.ratingValue = 0;
  menu.ratingMin = 0;
  menu.ratingMax = 5;
  menu.categories = wooData.categoryInfo;
  //variables for review
  menu.reviewValue = 0;
  menu.reviewMin = 0;
  menu.reviewMax = 500;

  //variables for Price
  // menu.priceValue = 0;
  menu.selectedWebsite = 'All';

  function ObjecttoParams(obj) {
    var p = [];
    for (var key in obj) {
      p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    console.log(p.join('&'))
    return p.join('&');
  };

  var cart = window.localStorage.getItem(Shop.name + "-cart");
  cart = JSON.parse(cart);

  if (cart && cart.length > 0) {
    $scope.totalCartItem = 0;
    for (var i in cart)
      $scope.totalCartItem += cart[i].qty;
  }

  // $scope.$watch(angular.bind(menu.ratingValue, function () {
  //   return menu.ratingValue;
  // }), function (value) {
  //   menu.sliderOfStar = { 'left': 30 + 32 * menu.ratingValue + 'px' };
  // });
  // $scope.$watch(angular.bind(menu.reviewValue, function () {
  //   return menu.reviewValue;
  // }), function (value) {
  //   menu.sliderOfReviews = { 'left': 30 + .29 * menu.reviewValue + 'px' };
  // });
  // $scope.$watch(angular.bind(menu.priceValue, function () {
  //   return menu.priceValue;
  // }), function (value) {
  //   menu.sliderOfPrice = { 'left': 30 + .125 * menu.priceValue + 'px' };
  // });
  menu.priceChanged = function () {
    menu.sliderMin = { 'left': 10 + (menu.minPriceSelected * 180 / (menu.priceMax - menu.priceMin)) + 'px' };
    menu.sliderMax = { 'right': 10 + (180 - (menu.maxPriceSelected * 180 / (menu.priceMax - menu.priceMin))) + 'px' };
  }

  // sidemenu filter as modal
  $ionicModal.fromTemplateUrl('templates/filterOptions.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.modal = modal;
  });
  $rootScope.openModal = function () {
    $scope.modal.show();
    console.log('open modal')
  }
  $scope.closeModal = function () {
    $scope.modal.hide();
    console.log("close modal");
  };
  $scope.GoToCart = function (x) {
    $state.go('cart');
  }
  menu.toggleCategoryList = function () {
    menu.showCategoryList = !menu.showCategoryList;
  }
  menu.toggleSortList = function () {
    menu.showSortList = !menu.showSortList;
  }
  menu.toggleBrandList = function () {
    menu.showBrandList = !menu.showBrandList;
    console.log('brandlist toggled');
  }
  menu.toggleWebsiteList = function () {
    menu.showWebsiteList = !menu.showWebsiteList;
  }
  menu.toggleCategoryList = function () {
    menu.showCategoryList = !menu.showCategoryList;
  }
  menu.toggleSubCategoryList = function () {
    menu.showSubCategoryList = !menu.showSubCategoryList;
  }
  menu.selectCat = function (cat) {
    menu.selectedCatId = cat.term_taxonomy_id;
    menu.selectedCat = cat.name;
    menu.showCategoryList = false;
    paramService.categoryTitle = cat.name;
    paramService.categoryImage = cat.img;

    console.log(menu.selectedCatId + 'selected');
  }
  menu.selectSubCat = function (subcat) {
    menu.selectedSubCatId = subcat.term_taxonomy_id;
    menu.selectedSubCat = subcat.name;
    menu.showSubCategoryList = false;
    console.log(menu.selectedSubCatId + 'selected');
  }
  menu.selectSort = function (sort) {
    menu.selectedSort = sort.name;
    menu.selectedSortId = sort.id;
    menu.showSortList = false;
    console.log(menu.selectedSortId + 'selected');
  }
  menu.selectBrand = function (brand) {
    menu.selectedBrand = brand.name;
    menu.selectedBrandId = brand.term_taxonomy_id;
    menu.showBrandList = false;
    console.log(menu.selectedBrandId + 'selected');
  }
  menu.selectWebsite = function (website) {
    menu.selectedWebsite = website.logo;
    menu.showWebsiteList = false;
    console.log(menu.selectedWebsite + 'selected');
  }
  menu.hideShownList = function () {
    menu.showCategoryList = false;
    menu.showSubCategoryList = false;
    menu.showBrandList = false;
    menu.showWebsiteList = false;
    menu.showSortList = false;
    console.log('hide all');
  }
  menu.catHasSubCat = function (catId) {
    var index = _.findIndex(menu.allCategories, function (cat) {
      return cat.term_taxonomy_id == catId;
    })

    if (menu.allCategories[index].subcategory.length) {
      return true;
    }
    else {
      return false;
    }
  }
  $scope.GoToUserProfile = function () {

    console.log(AuthService.isAuthenticated());
    var user = window.localStorage.getItem(Shop.name + "-user");
    if (AuthService.isAuthenticated() && user) {
      $state.go('userProfile');
    }
    else {
      if (window.cordova) {
        var onConfirm = function (buttonIndex) {

          if (buttonIndex == 1) {
            paramService.signin = {};
            paramService.signin.prevState = 'category';
            $state.go('signIn');
          }
          else {
            console.log('dismissed');
          }
        };
        navigator.notification.confirm(
          'Only logged in users can see their profile', // message
          onConfirm,            // callback to invoke with index of button pressed
          'Not logged in !',           // title
          ['Login', 'Dismiss']     // buttonLabels
        );
      }
      else {
        var r = confirm('Only logged in users can see their profile. Want to login?');
        if (r == true) {
          paramService.signin = {};
          paramService.signin.prevState = 'category';
          $state.go('signIn');

        } else {
          console.log('dismissed');
        }
      }
    }

  };
  menu.resetFilter = function () {
    // menu.rating = 0;
    // menu.review = 0;
    menu.price = 0;
    menu.selectedWebsite = 'All';
    menu.selectedBrand = 'All';
    menu.selectedSubCat = 'All';
    // getCurrentCategory();
    menu.selectedSubCatId = 0;
    menu.selectedBrandId = 0;

    menu.selectedCatId = paramService.categoryName = 0;
    menu.selectedCat = paramService.categoryTitle = 'All';
    paramService.categoryImage = 'img/allcat.jpg';
    // menu.ratingValue = 0;
    // menu.reviewValue = 0;
    // menu.priceValue = 0;
    menu.minPriceSelected = paramService.priceFilter.min;
    menu.maxPriceSelected = paramService.priceFilter.max;
    FilteredData.options = {};
    FilteredData.data = [];
    menu.applyFilter();

  }

  function filterByWebsite(data) {
    var filtered = [];
    _.each(data, function (item) {
      if (item.aff_data.length) {
        _.each(item.aff_data, function (aff) {
          if (aff.domain.indexOf(menu.selectedWebsite) != -1 || menu.selectedWebsite == "All") {
            filtered.push(item);
          }
        })
      }
      else if (menu.selectedWebsite == "All") {
        filtered.push(item);
      }
    })
    filtered = _.uniq(filtered);
    return filtered;
  }

  $scope.$on('textSearchCancelled', function () {
    menu.applyFilter('');
  })

  menu.go = function (searchText, clear) {
    FilteredData.data = [];
    FilteredData.options = {};
    FilteredData.options.skip = 1;
    menu.applyFilter(searchText, clear);
  }

  menu.applyFilter = function (searchText, clear) {
    //First load on page
    $ionicLoading.show();
    if (_.isEmpty(FilteredData.options)) {
      FilteredData.options = {
        q: '',
        minp: menu.minPriceSelected,
        maxp: menu.maxPriceSelected,
        sort: menu.selectedSortId,
        cat: angular.copy(paramService.categoryName),
        brand: menu.selectedBrandId,
        subcat: menu.selectedSubCatId,
        count: 10,
        skip: 1
      };
    }
    else if (!(_.isEmpty(FilteredData.options))) {
      var page = FilteredData.options.skip;

      //Coming back from product page or infinite scroll call of first load
      if (!searchText && !clear) {
        //Filter options stay same
      }
      else {
        FilteredData.options = {
          q: searchText || '',
          minp: menu.minPriceSelected,
          maxp: menu.maxPriceSelected,
          sort: menu.selectedSortId,
          cat: menu.selectedCatId,
          brand: menu.selectedBrandId,
          subcat: menu.selectedSubCatId,
          count: 10,
          skip: page
        };
      }
    }

    console.log(FilteredData.options);
    $http({
      method: "POST",
      url: customBackend + '/filter',
      data: ObjecttoParams(FilteredData.options),
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then(function (success) {
      console.log(success);
      var tempArray = filterByWebsite(success.data.products);
      if (tempArray.length < 10) {
        $rootScope.moreDataAvailable = false;
      }
      else {
        $rootScope.moreDataAvailable = true;
      }

      console.log(tempArray);
      tempArray = processMinPrices(tempArray);


      _.each(tempArray, function (item) {
        FilteredData.data.push(item);
      })

      if (menu.selectedSort == 'Newest first') {
        FilteredData.data = _.sortBy(FilteredData.data, ['ID', 'post_title'])
      }

      

      $rootScope.$broadcast('filterApplied');
      $ionicLoading.hide();
    }, function (err) {
      console.log(err);
    })
  }

  $scope.$on('searchWithFilters', function () {
    menu.applyFilter();
  })
  $scope.$on('infiniteScrollCalled', function () {
    FilteredData.options.skip++;
  })

  function processMinPrices(success) {
    var affiliateSpecificPrices = [];

    if (success.length) {
      $timeout(function () {
        // allProducts.searchResult = success.data.products;
        _.each(success, function (product) {
          affiliateSpecificPrices = [];
          if (product.aff_data && product.aff_data.length) {
            _.each(product.aff_data, function (aff) {
              if (aff.currencyCode && (aff.price || aff.price == 0 || aff.price == '0')) {
                affiliateSpecificPrices.push(convertCurrency.convert(aff.price, aff.currencyCode));
              }
            })
            if (affiliateSpecificPrices.length) {
              product.actualMinPrice = _.min(affiliateSpecificPrices);
            }
            else if (product.price) {
              product.actualMinPrice = convertCurrency.convert(product.price, 'USD');
            }
            else {
              product.invalid = true;
            }
          }
          else if (product.price) {
            product.actualMinPrice = convertCurrency.convert(product.price, 'USD');
          }
          else {
            product.invalid = true;
          }
        })
      }, 20);
      // _.each(success.data.products, function (product) {
      //   allProducts.searchResult.push(product);
      // })
    } else {
      if (window.cordova)
        $cordovaToast.showShortBottom('No products found. Please modify search');
    }

    $ionicLoading.hide();
    return success;
  }
}
