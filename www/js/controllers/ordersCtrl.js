angular.module('starter')
  .controller('ordersCtrl', ordersCtrl);

function ordersCtrl($scope, $state, $ionicLoading, $localForage, WC, Shop, AuthService, $timeout, $stateParams, $ionicHistory) {
  console.log("orders ctrl is working");
  var orders = this;
  console.log($stateParams.flag);

  if ($stateParams.flag) {
    orders.flag = $stateParams.flag;
  }
  else {
    orders.flag = 0;
    console.log(orders.flag);
  }

  var LOCAL_TOKEN_KEY = Shop.name + "-orders";
  $scope.user = {
    id: AuthService.id(),
    username: AuthService.username(),
    email: AuthService.email(),
    name: AuthService.name(),
    isLogin: AuthService.isAuthenticated()
  };

  $scope.showError = function (x, time) {
    var time = time ? time : 4000;
    $ionicLoading.show({
      template: '<div class="info"><i class="icon err ion-sad-outline"></i></div><div>' + x + '</div>'
    });
    $timeout(function () {
      $ionicLoading.hide();
    }, time);
  };

  $scope.doRefresh = function () {
    page = 1;
    $ionicLoading.show();
    WC.api().get('customers/' + $scope.user.id + '/orders?page=' + page + '&fields=id,created_at,total,line_items,status', function (err, data, res) {
      if (err) {
        $ionicLoading.hide();
        $scope.showError("Error in connection. Please try again.");
        return false;
      }
      $scope.order = JSON.parse(res).orders;
      $localForage.setItem(LOCAL_TOKEN_KEY, $scope.order)
      console.log($scope.order)
      console.log($scope.order.length)
      $ionicLoading.hide();
    })
  }

  if ($stateParams.flag) {
    $scope.doRefresh();
  }
  $localForage.getItem(LOCAL_TOKEN_KEY).then(function (orderHistory) {
    console.log(orderHistory);
    if (orderHistory) {
      console.log(orderHistory);
      $scope.order = orderHistory;
      console.log($scope.order.length)
    }
  })

  $scope.GoToOrderDetail = function (x) {
    $state.go('orderDetail', { id: x })
  }

  orders.GoToHome = function () {
    $state.go('searchProduct');         //thid is home state yet
  }

  orders.goBack = function () {
    $state.go('userProfile');         //thid is home state yet
  }

  orders.GoToCart = function (x) {
    $timeout(function () {
      $state.go('cart', { prevState: 'orders' });
    }, 1)

  }
}
