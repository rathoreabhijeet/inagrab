angular.module('starter')
  .controller('confirmCtrl',confirmCtrl);

function confirmCtrl($scope, $state, $ionicLoading, paramService, userBasicInfo, WC, Shop){
  console.log("confirm ctrl is working");
  var confirm=this;
  var order = JSON.parse(window.localStorage.getItem(Shop.name+"-order"));
  var cart = JSON.parse(window.localStorage.getItem(Shop.name+"-cart"));

  console.log(order);
  console.log(cart);

  var line_items = [], subtotal = 0;

  $scope.shipmethod    = order.shipping_lines[0].method_title;
  $scope.paymentmethod = order.payment_details.method_title;

  for(var i in cart){
    subtotal += cart[i].price * cart[i].qty;
    if(cart[i].variations){
      line_items.push({
        product_id: cart[i].id,
        quantity: 1,
    //    variations: {
    //      //name: cart[i].variations.name,
    //      //option: cart[i].variations.option
    //      [cart[i].variations.name]: cart[i].variations.option
    //}
    });
    }
    else line_items.push({product_id: cart[i].id, quantity: cart[i].qty});
  }

  $scope.subtotal = subtotal;
  console.log(subtotal);
  $scope.shipcost = order.shipping_lines[0].total;
  $scope.total    = $scope.subtotal + $scope.shipcost;

  order.line_items = line_items;
  console.log(order);

  $scope.createOrder = function(){
    $ionicLoading.show();
    var data = {order: order};
    WC.api().post('orders', data, function(err, data, res){
      var q = JSON.parse(res);

      if(err){
        $ionicLoading.hide();
        $scope.showError("Error in connection. Try again.");
        return false;
        $state.go('checkout');
      }

      var order = JSON.parse(res).order;
           console.log(order);
      window.localStorage.removeItem(Shop.name+"-cart");
      window.localStorage.removeItem(Shop.name+"-order");
      userBasicInfo.ship=null;
      userBasicInfo.billing=null;

        $ionicLoading.hide();
        paramService.orderId=order.id;
        paramService.orderTotal=order.total;
        paramService.payment=order.payment_details.method_id;
        $state.go('thanks',{id: order.id, total: order.total, payment: order.payment_details.method_id});

    })
  }
  confirm.goBack = function(){
    $state.go('shippingAddress');
  }
  }
