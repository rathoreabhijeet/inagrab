angular.module('starter')
  .controller('bestDealsCtrl', bestDealsCtrl);
function bestDealsCtrl(bestDealsData, $ionicLoading, $http, WC, paramService, $state, $timeout, $rootScope, $stateParams,
  transformRequestAsFormPost, customBackend, AffiliateData, convertCurrency, $cordovaToast, $ionicScrollDelegate, $scope,
  BestDealsResults) {
  var bestDeal = this;
  var resultsPage = 0;
  bestDeal.title = 'bestDealsCtrl';
  bestDeal.moreProductsAvailable = true;
  bestDeal.searchResult = [];
  // bestDeal.productList = [];
  console.log(paramService);
  bestDeal.symbol = paramService.selectedCurrency.symbol

  bestDeal.searchText = paramService.bestDeal.bestDealParam1;
  bestDeal.affiliateSpecificPrices = [];
  if (paramService.bestDeal.bestDealParam2) {
    bestDeal.selectedAffiliate = paramService.bestDeal.bestDealParam2;
  }
  console.log(bestDeal.selectedAffiliate);

  function ObjecttoParams(obj) {
    var p = [];
    for (var key in obj) {
      p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
  };

  bestDeal.findProduct = function () {
    bestDeal.noSearchResult = false;

    if (bestDeal.searchText !== '') {
      $ionicLoading.show({ template: 'Searching..' });
      $http({
        method: "POST",
        url: customBackend + '/search',
        data: ObjecttoParams({
          q: bestDeal.searchText,
          count: 10,
          skip: resultsPage
        }),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).then(function (success) {
        console.log(success);
        if (success.data.products && success.data.products.length) {
          if (success.data.products.length < 10) {
            bestDeal.moreProductsAvailable = false;
          }
          $timeout(function () {
            _.each(success.data.products, function (product) {
              bestDeal.searchResult.push(product);
            })

            if (bestDeal.affiliateSelected) {

              bestDeal.searchResult = _.filter(bestDeal.searchResult, function (product) {
                var productExists = false;
                bestDeal.affiliateSpecificPrices = [];

                if (product.aff_data && product.aff_data.length) {
                  _.each(product.aff_data, function (aff) {
                    if (aff.type == 'product' && aff.domain.indexOf(searchProduct.affiliateSelected.logo) !== -1) {
                      if (aff.currencyCode && (aff.price || aff.price == 0 || aff.price == '0')) {
                        console.log('true');
                        productExists = true;
                        bestDeal.affiliateSpecificPrices.push(convertCurrency.convert(aff.price, aff.currencyCode));
                      }
                    }
                  })
                  product.actualMinPrice = _.min(bestDeal.affiliateSpecificPrices);
                }
                return productExists;
              });
            }
            else {
              _.each(bestDeal.searchResult, function (product) {
                bestDeal.affiliateSpecificPrices = [];
                if (product.aff_data && product.aff_data.length) {
                  _.each(product.aff_data, function (aff) {
                    if (aff.type == 'product' && aff.currencyCode && (aff.price || aff.price == 0 || aff.price == '0')) {
                      bestDeal.affiliateSpecificPrices.push(convertCurrency.convert(aff.price, aff.currencyCode));
                    }
                  })
                  if (bestDeal.affiliateSpecificPrices.length) {
                    product.actualMinPrice = _.min(bestDeal.affiliateSpecificPrices);
                  }
                  else if (product.price) {
                    product.actualMinPrice = convertCurrency.convert(product.price, 'USD');
                  }
                  else {
                    product.invalid = true;
                  }
                }
                else if (product.price) {
                  product.actualMinPrice = convertCurrency.convert(product.price, 'USD');
                }
                else {
                  product.invalid = true;
                }
              })
            }

            if (bestDeal.searchResult.length) {
              bestDeal.searchResults = true;
            }
            else {
              bestDeal.noSearchResult = true;
              if (window.cordova)
                $cordovaToast.showShortBottom('No products found. Please modify search');
            }

          }, 20);

          $timeout(function () {
            $ionicScrollDelegate.resize();
            $scope.$broadcast('scroll.infiniteScrollComplete');
          }, 200)
        } else {
          bestDeal.noSearchResult = true;
          if (window.cordova)
            $cordovaToast.showShortBottom('No products found. Please modify search');

          $timeout(function () {
            $ionicScrollDelegate.resize();
            $scope.$broadcast('scroll.infiniteScrollComplete');
          }, 200)
        }

        $ionicLoading.hide();
      }, function (error) {
        console.log(error);
        if (window.cordova)
          $cordovaToast.showShortBottom('No products found. Please modify search');
        $ionicLoading.hide();
      })
    }
  };
  bestDeal.productDesc = function (selectedItem) {
    console.log("selectedItem", selectedItem);
    $ionicLoading.show({ template: 'Fetching product details ...' });
    // WC.api().get('products/' + selectedItem.ID, function (err, data, res) {
    //   // console.log(data);
    //   // console.log(res);
    //   if (err) {
    //     console.log(err);
    //     $ionicLoading.hide();
    //   } else {
    //     if (JSON.parse(res).errors) {
    //       console.log(JSON.parse(res).errors[0].message);
    //       $ionicLoading.hide();
    //     } else {
    //       console.log(JSON.parse(res).product);
    paramService.productDetail = selectedItem;
    paramService.productDetail.fromSearch = 'bestDeals';
    BestDealsResults.data = bestDeal.searchResult;
    $state.go('productDetail');
    $ionicLoading.hide();
    //     }
    //   }
    // });
  };
  bestDeal.GoBack = function () {
    $state.go(paramService.bestDeal.bestDealParam3);
  }

  bestDeal.fetchMoreResults = function () {
    console.log('infinite');
    if (BestDealsResults.data.length) {
      $timeout(function () {
        bestDeal.searchResult = BestDealsResults.data;
        BestDealsResults.data = [];
        console.log(bestDeal.searchResult);
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }, 200)

      //  $scope.$broadcast('scroll.infiniteScrollComplete');
    }
    else {
      if (bestDeal.searchResult.length && bestDeal.searchResult.length % 10 == 0) {
        resultsPage++;
        bestDeal.findProduct();
      }
      else {
        bestDeal.moreProductsAvailable = false;
        if (window.cordova) {
          $cordovaToast.showShortCenter('No more search results for this search');
        }
        else {
          alert('No more search results for this search');
        }
      }
    }
  }

  // bestDeal.findProduct();

}
