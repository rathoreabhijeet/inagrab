angular.module('starter')
  .controller('loginCtrl', loginCtrl);

function loginCtrl($window, $state, $q, $rootScope, backend, $localForage, user, 
$http, Shop, AuthService, $scope, $ionicLoading, Flags, paramService, $timeout, $cordovaToast) {
  var login = this;
  var deviceWidtht = $window.innerWidth;
  var deviceHeight = $window.innerHeight;

  console.log("dev height" + (deviceHeight / 100) * 5);
  console.log("dev width" + deviceWidtht);
  login.margin5percent = { 'margin-top': (deviceHeight / 100) * 4 + '%' };
  console.log("login ctrl is working");
  console

  var LOCAL_TOKEN_KEY = Shop.name + "-user";
  var user = JSON.parse(window.localStorage.getItem(LOCAL_TOKEN_KEY));
  if (user) {
    console.log(user);
    if (user) {
      console.log("user all ready loggedin");
      $state.go('category');
    }
  }


  // ----------------------------------FACEBOOK LOGIN----------------------------------- //

  // This method is to get the user profile info from the facebook api
  login.user = {};
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
      //
      function (response) {
        info.resolve(response);
      },
      function (response) {
        info.reject(response);
        $ionicLoading.hide();
      }
    );
    return info.promise;
  };

  login.userExists = function (info) {
    var obj = {
      name: info.email,
      password: info.password
    }
    AuthService.login(obj)
      .then(function (z) {
        console.log('yes')
        $localForage.setItem('userloginInfo', info);

        $state.go('category', {}, { reload: true });
        $ionicLoading.hide();
      },
      function () {
        console.log('no')
        login.addUser(info);
      })
  }


  login.addUser = function (info) {
    console.log(info);
    $http.get(Shop.URL + "/api/get_nonce/?controller=user&method=register")
      .success(function (getNonce) {
        console.log(getNonce)
        if (getNonce && getNonce.nonce) {
          console.log(info);
          $http.get(Shop.URL + "/api/user/register/?username=" + info.email + "&nonce=" + getNonce.nonce + "&email=" + info.email + "&first_name=" + info.firstName +
            "&last_name=" + info.lastName + "&user_pass=" + info.password + "&insecure=cool&display_name=" + info.firstName + " " + info.lastName)
            .success(function (userRegisterRes) {
              console.log(userRegisterRes);
              if (userRegisterRes.status == 'ok') {
                var login = {
                  name: info.email,
                  password: info.password,
                  email: info.email
                }
                $localForage.setItem('userloginInfo', info);
                $timeout(function () {
                  AuthService.login(login)
                    .then(function (z) {
                      $ionicLoading.hide();
                      console.log("in Authservice success");

                      if (Flags.redirectFormCart) {
                        $state.go('shippingAddress')
                      }
                      else {
                        Flags.redirectFormCart = 0;
                        $state.go('category', {}, { reload: true })
                      }
                    }, function (err) {
                      $ionicLoading.hide();
                      console.log("in Authservice err")
                      console.log(err);
                      $scope.message = err;
                    });
                }, 1000)

              } else {
                $ionicLoading.hide();
                console.log("in status else part status!=ok")
                $scope.message = userRegisterRes.error;
              }
            })
            .error(function (err) {
              console.log(info);
              console.log(err);
              $ionicLoading.hide();
              $scope.message = err.error;
            });
        } else {
          $ionicLoading.hide();
          $scope.message = getNonce.error;
        }
      })
      .error(function (err) {
        $ionicLoading.hide();
        $scope.message = "Error in connection. Please try again";
      });
  }

  // This is the success callback from the login method
  var fbLoginSuccess = function (response) {
    console.dir(response);
    if (!response.authResponse) {
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
      .then(function (profileInfo) {
        $ionicLoading.show();
        console.log(profileInfo);
        var lengthOfString = profileInfo.name.length;
        var SpaceInString = profileInfo.name.search(" ");
        var firstName = profileInfo.name.slice(0, SpaceInString);
        var lastName = profileInfo.name.slice(SpaceInString, lengthOfString);

        var userInfo = {};
        userInfo.email = profileInfo.email;
        userInfo.username = profileInfo.name;
        userInfo.password = profileInfo.id;
        userInfo.lastName = lastName;
        userInfo.firstName = firstName;
        userInfo.picture = "http://graph.facebook.com/" + profileInfo.id + "/picture?type=normal",

          login.userExists(userInfo);

      })

  };


  // This is the fail callback from the login method
  var fbLoginError = function (error) {
    console.log(error);
    $ionicLoading.hide();
  };

  //This method is executed when the user press the "Login with facebook" button
  login.facebookSignIn = function () {
    if (window.cordova) {
      $ionicLoading.show();
      facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
    }
    else {
      console.log("facebook login work only on mobile");
    }
  };


  login.GoToSignIn = function () {
    $state.go('signIn');
  }
  login.GoTosignUpForm = function () {
    $state.go('signUp');
  }

  // ------------------------------------------GOOGLE SIGNIN--------------------------------------------//
  login.googleLogin = function () {
    $ionicLoading.show();
    if(device.platform=='iOS'){
    var loginObject = {
        // 'scopes': 'profile ', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        // 'webClientId': '531708090504-lsrfut5k0937hn80b0fm0qv23fggjq65.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
        // 'offline': true, // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
      }
    }
    else{
      var loginObject = {
        'scopes': 'profile ', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        'webClientId': '531708090504-te572m2p3fg3p56lg14atil2m5c8482j.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
        'offline': true, // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
      }
    }
    window.plugins.googleplus.login(
      loginObject,
      function (obj) {
        //alert(JSON.stringify(obj)); // do something useful instead of alerting
        console.log(obj);
        var userInfo = {};
        userInfo.email = obj.email;
        userInfo.username = obj.displayName;
        userInfo.password = obj.userId;
        userInfo.firstName = obj.givenName;
        userInfo.lastName = obj.familyName;
        userInfo.picture = obj.imageUrl;
        userInfo.id = obj.idToken;
        userInfo.serverAuth = obj.serverAuthCode;
        // userInfo.picture = obj.imageUrl;

        console.log(userInfo);
        // $localForage.setItem('userInfoByGoogleLogin', userInfo);
        login.userExists(userInfo);

      },
      function (msg) {
        console.log('Error in login' + msg);
        $ionicLoading.hide();
        $cordovaToast.showShortBottom('Error in Facebook login');
      }
    );
  }

  login.forgotPassword = function () {
    var options =
      {
        title: 'Enter your e-mail',
        subTitle: 'You will get a one-time password on mail',
        cssClass: 'addCommentPopup',
        template: '<forgot-password></forgot-password>'
      }
    forgotPasswordPopup = $ionicPopup.alert(options);

    IonicClosePopupService.register(forgotPasswordPopup);
  }



  login.goToHome = function () {
    Flags.redirectFormCart = 0;
    $state.go('searchProduct')
  }

}
