angular.module('starter')
  .controller('signUpCtrl',signUpCtrl);

function signUpCtrl($state,$scope,AuthService,Shop,$http,$ionicLoading){
  var signUp=this;
  $scope.userDetail={
    firstName:'',
    lastName:'',
    username:'',
    number:'',
    email:'',
    password:''
  };

  console.log("SignUp ctrl is working");

  var flag=window.localStorage.getItem("redirectFromCart");
  console.log(flag);

  signUp.GoBack = function(){
    $state.go('login')
  }
  signUp.GoToCategory = function(){
    $ionicLoading.show();
    console.log($scope.userDetail);
    $http.get(Shop.URL+"/api/get_nonce/?controller=user&method=register")
      .success(function(x){
        console.log(x)
        if(x.nonce){
          //console.log($scope.u);
          console.log($scope.userDetail);
          $http.get(Shop.URL+"/api/user/register/?username="+$scope.userDetail.username+"&nonce="+x.nonce+"&email="+$scope.userDetail.email+"&first_name="+$scope.userDetail.firstName+
            "&last_name="+$scope.userDetail.lastName+"&user_pass="+$scope.userDetail.password+"&insecure=cool&display_name="+$scope.userDetail.firstName+" "+$scope.userDetail.lastName)
            .success(function(y){
              console.log(y);
              if(y.status=='ok'){
                var login = {
                  name: $scope.userDetail.username,
                  password: $scope.userDetail.password,
                  email:$scope.userDetail.email,
                }
                AuthService.login(login)
                  .then(function(z){
                    //$ionicLoading.hide();
                    console.log("in Authservice success")
                    $ionicLoading.hide();
                    if(flag){
                      $state.go('shippingAddress');
                    }
                    else {
                      $state.go('category', {}, {reload: true})
                    }
                  }, function(err){
                    $ionicLoading.hide();
                    console.log("in Authservice err")
                    console.log(err);
                    $scope.message = err;
                  });
              }else{
                $ionicLoading.hide();
                console.log("in status else part status!=ok")
                $scope.message = y.error;
              }
            })
            .error(function(err){
              $ionicLoading.hide();
              console.log($scope.userDetail);
              console.log(err);
              $scope.message = err.error;
            });
        }else{
          $ionicLoading.hide();
          $scope.message = x.error;
        }
      })
      .error(function(err){
        $ionicLoading.hide();
        $scope.message = "Error in connection. Please try again";
      });
    //$state.go('category')

  }
}
