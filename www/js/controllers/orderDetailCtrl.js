angular.module('starter')
  .controller('orderDetailCtrl',orderDetailCtrl);

function orderDetailCtrl($scope, $stateParams, $ionicLoading,WC){
  console.log("orderDetail ctrl is working");
  var orderDetail=this;
  $scope.id = $stateParams.id;
  console.log($stateParams.id);
  $ionicLoading.show();

  WC.api().get('orders/'+$stateParams.id, function(err, data, res){
    if(err){
      $scope.showError("Error in connection. Please try again.");
      $ionicLoading.hide();
      return false;
    }
    $scope.order = JSON.parse(res).order;
    $ionicLoading.hide();
  })

}
