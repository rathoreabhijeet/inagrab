angular.module('starter')
  .controller('searchProductCtrl', searchProductCtrl);

function searchProductCtrl($window, $state, AuthService, Shop, $scope, $http, backend, $timeout,
  paramService, WC, $ionicLoading, bestDealsData, $cordovaToast, transformRequestAsFormPost, customBackend,
  AffiliateData, $rootScope, convertCurrency, BestDealsResults) {
  console.log("searchProduct ctrl is working");
  paramService.selectedCurrency = { 'currencyCode': 'USD', 'rate': 1, 'name': 'US Dollar', 'symbol': '$' };;

  var searchProduct = this;
  var deviceWidtht = $window.innerWidth;
  var deviceHeight = $window.innerHeight;
  searchProduct.swiper = {};
  searchProduct.swiper.centeredSlides = "true";
  searchProduct.main = { 'height': 0.7 * deviceHeight + 'px' };
  searchProduct.searchItemList = { 'max-width': deviceWidtht + 'px' };
  searchProduct.affiliateData = AffiliateData.data;
  searchProduct.searchFromAll = true;
  searchProduct.symbol = paramService.selectedCurrency.symbol;
  BestDealsResults.data.length = 0;
  searchProduct.prev = function () {
    searchProduct.swiper.slidePrev();
  }

  var cart = window.localStorage.getItem(Shop.name + "-cart");
  cart = JSON.parse(cart);

  if (cart && cart.length > 0) {
    $scope.totalCartItem = 0;
    for (var i in cart)
      $scope.totalCartItem += cart[i].qty;
  }

  document.addEventListener("deviceready", function () {
    if (window.cordova) {
      navigator.splashscreen.hide();
    }
  });

  searchProduct.next = function () {
    searchProduct.swiper.slideNext();
  }

  searchProduct.GoToCategory = function () {
    $state.go('category');
  }
  searchProduct.GoToCart = function () {
    $state.go('cart', { prevState: 'searchProduct' });
  }
  searchProduct.GoToUserProfile = function () {
    console.log(AuthService.isAuthenticated());
    var user = window.localStorage.getItem(Shop.name + "-user");
    if (AuthService.isAuthenticated() && user) {
      $state.go('userProfile');
    }
    else {
      if (window.cordova) {
        var onConfirm = function (buttonIndex) {

          if (buttonIndex == 1) {
            paramService.signin = {};
            paramService.signin.prevState = 'searchProduct';
            $state.go('signIn');
          }
          else {
            console.log('dismissed');
          }
        };
        navigator.notification.confirm(
          'Only logged in users can see their profile', // message
          onConfirm,            // callback to invoke with index of button pressed
          'Not logged in !',           // title
          ['Login', 'Dismiss']     // buttonLabels
        );
      }
      else {
        var r = confirm('Only logged in users can see their profile. Want to login?');
        if (r == true) {
          paramService.signin = {};
          paramService.signin.prevState = 'searchProduct';
          $state.go('signIn');

        } else {
          console.log('dismissed');
        }
      }
    }
  };

  function ObjecttoParams(obj) {
    var p = [];
    for (var key in obj) {
      p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
  };


  searchProduct.findProduct = function () {
    var searchPage = 1;
    searchProduct.noSearchResult = false;
    searchProduct.searchResult = [];
    if (searchProduct.searchText !== '') {
      $ionicLoading.show({ template: 'Searching..' });
      $http({
        method: "POST",
        url: customBackend + '/search',
        data: ObjecttoParams({
          q: searchProduct.searchText,
          count: 10,
          skip: 1
        }),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).then(function (success) {
        console.log(success);
        if (success.data.products && success.data.products.length) {

          $timeout(function () {
            searchProduct.searchResult = success.data.products;
            if (searchProduct.affiliateSelected) {
              searchProduct.searchResult = _.filter(searchProduct.searchResult, function (product) {
                var productExists = false;
                searchProduct.affiliateSpecificPrices = [];

                if (product.aff_data && product.aff_data.length) {
                  _.each(product.aff_data, function (aff) {
                    if (aff.type == 'product' && aff.domain.indexOf(searchProduct.affiliateSelected.logo) !== -1) {
                      if (  aff.currencyCode && (aff.price || aff.price == 0 || aff.price == '0')) {
                        console.log('true');
                        productExists = true;
                        searchProduct.affiliateSpecificPrices.push(convertCurrency.convert(aff.price, aff.currencyCode));
                      }
                    }
                  })
                  product.actualMinPrice = _.min(searchProduct.affiliateSpecificPrices);
                }
                return productExists;
              });
            }
            else {
              _.each(searchProduct.searchResult, function (product) {
                searchProduct.affiliateSpecificPrices = [];
                if (product.aff_data && product.aff_data.length) {
                  _.each(product.aff_data, function (aff) {
                    if (aff.type == 'product' && aff.currencyCode && (aff.price || aff.price == 0 || aff.price == '0')) {
                      console.log(aff.price);
                      searchProduct.affiliateSpecificPrices.push(convertCurrency.convert(aff.price, aff.currencyCode));
                    }
                  })
                  if (searchProduct.affiliateSpecificPrices.length) {
                    product.actualMinPrice = _.min(searchProduct.affiliateSpecificPrices);
                  }
                  else if (product.price) {
                    product.actualMinPrice = convertCurrency.convert(product.price, 'USD');
                  }
                  else {
                    product.invalid = true;
                  }
                }
                else if (product.price) {
                  product.actualMinPrice = convertCurrency.convert(product.price, 'USD');
                }
                else {
                  product.invalid = true;
                }
              })
            }

            if (searchProduct.searchResult.length) {
              searchProduct.searchResults = true;
            }
            else {
              searchProduct.noSearchResult = true;
              if (window.cordova)
                $cordovaToast.showShortBottom('No products found. Please modify search');
            }
          }, 20);
        } else {
          searchProduct.noSearchResult = true;
          console.log('no products found');
          if (window.cordova)
            $cordovaToast.showShortBottom('No products found. Please modify search');
        }

        $ionicLoading.hide();
      }, function (error) {
        console.log(error);
        if (window.cordova)
          $cordovaToast.showShortBottom('No products found. Please modify search');
        $ionicLoading.hide();
      })
    }

  };
  searchProduct.productDesc = function (selectedItem) {
    console.log("selectedItem", selectedItem);
    $ionicLoading.show({ template: 'Fetching product details ...' });
    // WC.api().get('products/' + selectedItem.ID, function (err, data, res) {
    //   // console.log(data);
    //   // console.log(res);
    //   if (err) {
    //     console.log(err);
    //     $ionicLoading.hide();
    //   } else {
    //     if (JSON.parse(res).errors) {
    //       console.log(JSON.parse(res).errors[0].message);
    //       $ionicLoading.hide();
    //     } else {
    //       console.log(JSON.parse(res).product);
    paramService.productDetail = selectedItem;
    paramService.productDetail.fromSearch = 'searchProduct';
    $state.go('productDetail');
    $ionicLoading.hide();
    //     }
    //   }
    // });
  };
  searchProduct.bestDeals = function () {
    if (searchProduct.searchText) {
      // bestDealsData.productList = searchProduct.searchResult;
      paramService.bestDeal = {};
      paramService.bestDeal.bestDealParam1 = searchProduct.searchText;

      if (searchProduct.affiliateSelected) {
        paramService.bestDeal.bestDealParam2 = searchProduct.affiliateSelected.logo;
      }
      paramService.bestDeal.bestDealParam3 = 'searchProduct';
      BestDealsResults.data = searchProduct.searchResult;
      $state.go('bestDeals');

    }
    else {
      if (window.cordova) {
        if (!searchProduct.searchText) {
          $cordovaToast.showShortBottom('Please enter a search term first');
        }
        else if (!searchProduct.searchResult.length) {
          $cordovaToast.showShortBottom('No search results found. Please modify your search');
        }
      }
      else {
        if (!searchProduct.searchText) {
          alert('Please enter a search term first');
        }
        else if (!searchProduct.searchResult.length) {
          alert('No search results found. Please modify your search');
        }
      }
    }
  }

  searchProduct.focusInput = function () {
    searchProduct.keyboardOpen = true;
  }
  searchProduct.selectAffiliate = function (shop) {
    searchProduct.affiliateSelected = shop;
    searchProduct.searchFromAll = false;

    if (searchProduct.affiliateSelected.logo == 'all') {
      searchProduct.affiliateSelected = null;
      searchProduct.searchFromAll = true;
    }
    if (searchProduct.searchText) {
      searchProduct.findProduct();
    }
  }
  searchProduct.cancelSearch = function () {
    searchProduct.searchText = '';
    searchProduct.searchResult.length = 0;
  }
}
