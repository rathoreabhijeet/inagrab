var config = {
    "title": "inagrab",
    "link": "http://inagrab.com/",
    "checkout": "http://inagrab.com//checkout/",
    "timeout": "20000",
    "currency": "&#36;",
    "api": {
        "link": "http://inagrab.com/wp-json/wc/v1/",
        "key": "ck_b600036ba794276bebe45442cc2d516c71480e91",
        "secret": "cs_b9901fd0f8ab3e4a61872b9727c00ae51227a9dd",
        "signature": "HMAC-SHA1",
        "version": "1.0"
    },
    "messages": {
        "sale_text": "Sale",
        "added_cart": "Successfully added to your cart !",
        "in_stock": "in stock",
        "total": "Total:",
        "exists_cart": "Product already exist in your cart !",
        "server": "There was a problem connecting to the server ! Please try again later",
        "no_internet": "Please check your internet connection !",
        "empty": "There's no products in this categorie !",
        "empty_cart": "There's no item in your cart !",
        "empty_shop": "The shop is empty !",
        "error": "An unexpected error occured !"
    },
    "views": {"homePage": "Ma Boutique", "cart": "Cart"},
    "buttons": {
        "home": "Home",
        "add_cart": "Add To Cart",
        "buy": "Buy Now",
        "checkout": "Checkout",
        "empty": "Empty Cart",
        "cancel": "Cancel",
        "ok": "Ok"
    },
    "alerts": {
        "remove_cart": {
            "title": "Remove From Cart",
            "message": "Are you sure you want to remove this item from your cart ?"
        }, "empty_cart": {"title": "Empty Cart", "message": "Are you sure you want to empty your cart ?"}
    }
}
