// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in menuCtrl.js
angular.module('starter', ['ionic', 'ksSwiper', 'ngCordova', 'ngResource', 'ngCordovaOauth',
  'starter.controllers', 'base64', 'LocalForageModule', 'ngSanitize', 'ionic-cache-src'])

  .run(function ($ionicPlatform, $http, $rootScope, customBackend, CategoryData, paramService, $ionicLoading, $cordovaToast,
    $cordovaNetwork) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      if ($cordovaNetwork.isOnline) {


        $ionicLoading.show();
        $http.get('http://api.fixer.io/latest?base=USD').then(function (currencyData) {
          console.log(currencyData);
          $rootScope.currencyConversionData = currencyData.data.rates;
          console.log($rootScope.currencyConversionData);
        })

        // Fetch all cat/subcat and brands
        $http({
          method: "POST",
          url: customBackend + '/categorydata',
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function (success) {
          console.log(success);
          paramService.priceFilter = {};
          paramService.priceFilter.max = success.data.max_price;
          paramService.priceFilter.min = success.data.min_price;
          paramService.priceFilter.fixed_max = success.data.max_price;
          paramService.priceFilter.fixed_min = success.data.min_price;
          _.each(success.data.brands, function (brand) {
            brand.term_taxonomy_id = parseInt(brand.term_taxonomy_id);
            CategoryData.brands.push(brand);
          })
          _.each(success.data.category, function (cat) {
            cat.term_taxonomy_id = parseInt(cat.term_taxonomy_id)
            CategoryData.categories.push(cat);
            if (cat.subcategory.length) {
              _.each(cat.subcategory, function (subcat) {
                subcat.catId = parseInt(cat.term_taxonomy_id);
                subcat.term_taxonomy_id = parseInt(subcat.term_taxonomy_id);
                CategoryData.subcategories.push(subcat);
              })
            }
          })
          CategoryData.brands.unshift({ 'name': 'All', 'description': 'All brands', 'term_taxonomy_id': 0 });
          CategoryData.categories.unshift({ 'name': 'All', 'description': 'All categories', 'term_taxonomy_id': 0,'img':'img/allcat.jpg' });
          CategoryData.subcategories.unshift({ 'name': 'All', 'description': 'All subcategories', 'term_taxonomy_id': 0 });
          console.log(CategoryData);
          $ionicLoading.hide();

        },
          function (err) {
            console.log(err);
            $ionicLoading.hide();
            $cordovaToast.showShortBottom('No network detected. Please retry');
          })
      }
      else {
            $cordovaToast.showShortBottom('No network detected. Please retry');
      }
    });
  })
  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(0);
    if (ionic.Platform.isAndroid()) {
      $ionicConfigProvider.scrolling.jsScrolling(false);
    }
    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'menuCtrl',
        controllerAs: 'menu'
      })
      .state('login', {
        url: '/login/:prevState',
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl',
        controllerAs: 'login'
      })
      .state('signUp', {
        url: '/signUp',
        templateUrl: 'templates/signUp.html',
        controller: 'signUpCtrl',
        controllerAs: 'signUp'
      })
      .state('signIn', {
        url: '/signIn/:prevState',
        templateUrl: 'templates/signIn.html',
        controller: 'signInCtrl',
        controllerAs: 'signIn'
      })

      .state('app.allProducts', {
        url: '/allProducts',
        views: {
          'menuContent': {
            templateUrl: 'templates/allProducts.html',
            controller: 'allProductsCtrl',
            controllerAs: 'allProducts',
            params: {
              categoryName: null,
            }
          }
        }
      })
      .state('productDetail', {
        url: '/productDetail',
        templateUrl: 'templates/productDetail.html',
        controller: 'productDetailCtrl',
        controllerAs: 'productDetail',
        params: {
          selectedProduct: null,
        }
      })
      .state('reviews', {
        url: '/reviews/:link',
        templateUrl: 'templates/reviews.html',
        controller: 'reviewsCtrl',
        controllerAs: 'reviews'
      })
      .state('bestDeals', {
        url: '/bestDeals/:search/:affiliate/:prev',
        templateUrl: 'templates/bestDeals.html',
        controller: 'bestDealsCtrl',
        controllerAs: 'bestDeal'
      })
      .state('searchProduct', {
        url: '/searchProduct',
        templateUrl: 'templates/searchProduct.html',
        controller: 'searchProductCtrl',
        controllerAs: 'searchProduct'
      })
      .state('randomProducts', {
        url: '/randomProducts',
        templateUrl: 'templates/randomProducts.html',
        controller: 'randomProductsCtrl',
        controllerAs: 'randomProducts'
      })
      .state('userProfile', {
        url: '/userProfile',
        templateUrl: 'templates/userProfile.html',
        controller: 'userProfileCtrl',
        controllerAs: 'userProfile'
      })
      .state('invite', {
        url: '/invite',
        templateUrl: 'templates/invite.html',
        controller: 'inviteCtrl',
        controllerAs: 'invite'
      })
      .state('forgotPassword', {
        url: '/forgotPassword',
        templateUrl: 'templates/forgotPassword.html',
        controller: 'forgotPasswordCtrl',
        controllerAs: 'forgotPassword'
      })
      .state('cart', {
        url: '/cart/:prevState',
        templateUrl: 'templates/cart.html',
        controller: 'cartCtrl',
        controllerAs: 'cart'
      })
      .state('shippingAddress', {
        url: '/shippingAddress',
        templateUrl: 'templates/shippingAddress.html',
        controller: 'shippingAddressCtrl',
        controllerAs: 'shippingAddress'
      })
      .state('confirm', {
        url: '/confirm',
        templateUrl: 'templates/confirm.html',
        controller: 'confirmCtrl',
        controllerAs: 'confirm'
      })
      .state('orders', {
        url: '/orders',
        templateUrl: 'templates/orders.html',
        controller: 'ordersCtrl',
        controllerAs: 'orders',
        params: {
          flag: null,
        }
      })
      .state('orderDetail', {
        url: '/orderDetail',
        templateUrl: 'templates/orderDetail.html',
        controller: 'orderDetailCtrl',
        controllerAs: 'orderDetail',
        params: {
          id: null,
        }
      })
      .state('thanks', {
        url: '/thanks',
        templateUrl: 'templates/thanks.html',
        controller: 'thanksCtrl',
        controllerAs: 'thanks',
        params: {
          id: null,
          total: null,
          payment: null
        }
      })
      .state('category', {
        url: '/category',
        templateUrl: 'templates/category.html',
        controller: 'categoryCtrl',
        controllerAs: 'category'
      })


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('searchProduct');
  })
  .constant('Shop', {
    version: '2.1.2 beta',
    name: 'test',
    URL: 'http://inagrab.com',
    ConsumerKey: 'ck_43cae87b1e7e200ffdce415c64aa4fceac77f679', // Get this from your WooCommerce
    ConsumerSecret: 'cs_2f4b12b0c288c3357aa66bae325fab0bef9008ac', // Get this from your WooCommerce

    homeSlider: true, // If you dont want to use home slider, set it to FALSE
    CurrencyFormat: true, // If you want to use currency format, set it to TRUE
    shipping: [
      { id: 'flat_rate:4', name: 'Local Pickup', cost: 0 },
      { id: 'flat_rate:3', name: 'Flat Rate', cost: 5 },
      { id: 'flat_rate:2', name: 'Worldwide Flat Rate', cost: 15 }
    ],
    payment: [
      { id: 'cod', name: 'Cash on Delivery', icon: 'fa fa-money', desc: 'Pay with cash upon delivery.' },
      { id: 'bacs', name: 'Direct Bank Transfer', icon: 'fa fa-university', desc: 'You can pay using direct bank account' },
      { id: 'paypal', name: 'Paypal', icon: 'fa fa-cc-paypal', desc: 'You can pay via Paypal and Credit Card' }
    ],

    GCM_SenderID: 'xxxxxxxxxxxx', // Get this from https://console.developers.google.com

    OneSignalAppID: 'xxxxxxxxxxxx', // Get this from https://onesignal.com
    // Change this paypal sandbox with your own
    payPalSandboxClientSecretBase64: 'QVpqeUlTYnAxem1PaFowb19pQUczVzJJR2psejJodkVDLThjR29RN2ZYY01GTjlhZmFSdVcwWDFCMVBWU2drU3VUUVdPS3FNOU40TlRrT1A6RUVZTEtFamVNT0tqbHdXZEtYTXI2MEtRcVlZeDg1aC1aOTk0Nk1TdmhLbjEyNmtfUkpWZnpOZEc3V0dpNi14N3RKSlNjOUMxaUc5c2lKb0U=',
    payPalProductionClientSecretBase64: 'xxxxxxxxxxxx',

    //  You need to change this url to GO LIVE!
    payPalGetTokenURL: 'https://api.sandbox.paypal.com/v1/oauth2/token', // to go live, use this: https://api.paypal.com/v1/oauth2/token
    payPalMakePaymentURL: 'https://api.sandbox.paypal.com/v1/payments/payment', // to go live, use this: https://api.paypal.com/v1/payments/payment
    payPalReturnURL: 'http://localhost/success',
    payPalCancelURL: 'http://localhost/cancel'
  })
  .constant('backend', 'http://inagrab.com/wp-json/wp/v2')
  .constant('customBackend', 'http://inagrab.com/api/v1')
  .constant('imageBaseUrl','http://inagrab.com/wp-content/uploads/')
  .constant('productUrlRedirection','http://inagrab.com/?redirectAmzASIN=')
