angular.module('starter')

  .factory('signInData', function () {
    return [{ id: 1, name: "ravi ranjan", password: "abcd" },
    { id: 2, name: "guru", password: "efgh" },
    { id: 3, name: "ashraf ali", password: "qwer" },
    { id: 4, name: "mugembo", password: "1234" }]
  })
  .factory('userData', function ($q, user) {
    var usersInfo;
    var defer = $q.defer();
    user.query(function (data) {
      console.log("userData factory is working");
      console.log(data);
      usersInfo = data;
      defer.resolve(usersInfo);
    }, function (err) {
      console.log(err);
    })
    return defer.promise;
  })

  .factory('categoriesData', function ($q, categories, $http) {
    var categoriesInfo;
    var defer = $q.defer();
    var request = generateRequest('GET', 'products/categories', false, false);
    request = request + '&per_page=100'

    $http.get(request, { timeout: config.timeout })
      .success(function (json) {
        console.log(json);
        defer.resolve(json);
      }).error(function (err) {
        console.log(err);
        defer.reject();
      })
    return defer.promise;
  })

  .factory('allProductsData', function ($q, $http) {
    var productsInfo;
    var defer = $q.defer();
    var request = generateRequest('GET', 'products', false, false);
    $http.get(request, { timeout: config.timeout })
      .success(function (json) {
        defer.resolve(json);
      })
      .error(function (err) {
        defer.reject();
      })

    return defer.promise;
  })

  .factory('convertCurrency', function (paramService, $rootScope) {
    return {
      convert: function (price, currency) {
        if (price && currency) {
          console.log(price, currency);
          if (currency == paramService.selectedCurrency.currencyCode) {
            return parseFloat(price);
          }
          else if (paramService.selectedCurrency.currencyCode == 'USD') {
            console.log(price / $rootScope.currencyConversionData[currency]);
            return parseFloat((price / $rootScope.currencyConversionData[currency]).toFixed(2));
          }
          else if (currency == 'USD') {
            console.log(price * $rootScope.currencyConversionData[paramService.selectedCurrency.currencyCode]);
            return parseFloat((price * $rootScope.currencyConversionData[paramService.selectedCurrency.currencyCode]).toFixed(2));
          }
          else {
            console.log(((price / $rootScope.currencyConversionData[currency]) * $rootScope.currencyConversionData[paramService.selectedCurrency.currencyCode]));
            return parseFloat(((price / $rootScope.currencyConversionData[currency]) * $rootScope.currencyConversionData[paramService.selectedCurrency.currencyCode]).toFixed(2));
          }
        }
      }
    }
  })

  .factory('postsData', function ($q, posts) {
    var postsInfo;
    var defer = $q.defer();
    posts.query(function (data) {
      console.log("postsData factory is working");
      console.log(data);
      postsInfo = data;
      defer.resolve(postsInfo);
    }, function (err) {
      console.log(err);
    })
    return defer.promise;
  })

  .factory('user', function ($resource, backend) {
    return $resource(backend + '/users');
  })
  .factory('categories', function ($resource, backend) {
    return $resource(backend + '/Categories');
  })
  .factory('posts', function ($resource, backend, $rootScope) {
    return $resource(backend + '/posts');
  })

  .service('WC', function (Shop) {
    return {
      api: function () {
        var Woocommerce = new WooCommerceAPI.WooCommerceAPI({
          url: Shop.URL,
          queryStringAuth: true,
          consumerKey: Shop.ConsumerKey,
          consumerSecret: Shop.ConsumerSecret
        });
        return Woocommerce;
      }
    }
  })
  .factory('AuthService', function ($q, $http, WC, Shop, $state) {
    var LOCAL_TOKEN_KEY = Shop.name + "-user";
    var id = '';
    var username = '';
    var email = '';
    var name = '';
    var phone = '';
    var isAuthenticated = false;
    var role = '';
    var authToken;

    function loadUserCredentials() {
      var user = JSON.parse(window.localStorage.getItem(LOCAL_TOKEN_KEY));
      if (user) {
        console.log(user);
        user.image = 0;
        storeUserCredentials(user);
      }
    }

    function storeUserCredentials(user) {
      window.localStorage.setItem(LOCAL_TOKEN_KEY, JSON.stringify(user));
      useCredentials(user);
    }

    function useCredentials(user) {
      id = user.id;
      username = user.username;
      email = user.email;
      name = user.firstname + ' ' + user.lastname;

      isAuthenticated = true;
      authToken = JSON.stringify(user);
      //$http.defaults.headers.common['X-Auth-Token'] = JSON.stringify(user);
    }

    function destroyUserCredentials() {
      authToken = undefined;

      id = '';
      username = '';
      email = '';
      name = '';

      isAuthenticated = false;
      //$http.defaults.headers.common['X-Auth-Token'] = undefined;
      window.localStorage.removeItem(LOCAL_TOKEN_KEY);
    }

    var login = function (data) {
      return $q(function (resolve, reject) {
        $http.get(Shop.URL + '/api/user/generate_auth_cookie/?username=' + data.name + '&password=' + data.password + '&insecure=cool')
          .success(function (x) {
            console.log(x);
            if (x.status == 'ok') {
              storeUserCredentials(x.user);
              resolve(x.user);
            } else
              reject(x.error);
          })
          .error(function (err) {
            reject('Error in connection check your internet');
          });
      });
    };

    var logout = function (id, os) {
      destroyUserCredentials();
      //$http.get(API+'logout.php?id='+id+'&os='+os, null, function(){});
    };

    var isAuthorized = function (authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }
      return (isAuthenticated && authorizedRoles.indexOf(role) !== -1);
    };

    loadUserCredentials();

    return {
      login: login,
      logout: logout,
      isAuthorized: isAuthorized,
      isAuthenticated: function () { return isAuthenticated; },
      id: function () { return id; },
      name: function () { return name; },
      username: function () { return username; },
      email: function () { return email; },
      authToken: function () { return authToken; },
      role: function () { return role; }
    };
  })
  .factory('Data', function ($http, $q, Shop) {
    return {
      send: function (data, url) {
        return $q(function (resolve, reject) {
          $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function (obj) {
              var p, str;
              str = [];
              for (p in obj) {
                str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
              }
              return str.join('&');
            },
            data: data
          })
            .success(function (x) {
              //if(x.status)
              resolve(x);
              //else
              //reject(x);
            })
            .error(function (err) {
              reject(err);
            });
        })
      },
      get: function (x) {
        return $q(function (resolve, reject) {
          $http.get(Shop.URL + x).success(function (x) {
            resolve(x);
          }).error(function (err) {
            reject(err);
          });
        })
      }
    }
  })
  .service('Flags', function () {
    redirectFormCart = 0;
    categoryInfo = [{}];
    //productsInfo=[];
  })
  .service('paramService', function () {
    categoryName = {};
    categoryTitle = {};
    categoryImage = {};
    productDetail = {};
    signin = {};
    orderTotal = {};
    orderId = {};
    payment = {};
    bestDeal = {};
    selectedCurrency = {};
    priceFilter = {};

  })
  .factory('wooData', function () {
    return { productsInfo: [], categoryInfo: [] };
  })
  .factory('CategoryData', function () {
    return { categories: [], subcategories: [], brands: [] };
  })
  .factory('BestDealsResults', function () {
    return { data: [] };
  })
  .factory('AllProductsResults', function () {
    return { data: [] };
  })
  .factory('FilteredData', function () {
    return { data: [], savedData: [], options: {}, subOptions: {} };
  })
  .factory('AffiliateData', function () {
    return {
      data: [
        { 'logo': 'all', 'domain': 'all.png' },
        { 'logo': 'admitad', 'domain': 'admitad.png' },
        { 'logo': 'aliexpress', 'domain': 'aliexpress.png' },
        { 'logo': 'amazon', 'domain': 'amazon.png' },
        { 'logo': 'bestbuy', 'domain': 'bestbuy.png' },
        { 'logo': 'ebay', 'domain': 'ebay.png' },
        { 'logo': 'etsy', 'domain': 'etsy.png' },
        { 'logo': 'flipkart', 'domain': 'flipkart.png' },
        { 'logo': 'infibeam', 'domain': 'infibeam.png' },
        { 'logo': 'jadopado', 'domain': 'jadopado.png' },
        { 'logo': 'newegg', 'domain': 'newegg.png' },
        { 'logo': 'paytm', 'domain': 'paytm.png' },
        { 'logo': 'rakuten', 'domain': 'rakuten.png' },
        { 'logo': 'sharesale', 'domain': 'sharesale.png' },
        { 'logo': 'shopclues', 'domain': 'shopclues.png' },
        { 'logo': 'snapdeal', 'domain': 'snapdeal.png' },
        { 'logo': 'walmart', 'domain': 'walmart.png' },
        { 'logo': 'zanox', 'domain': 'zanox.png' }
      ]
    };
  })
  .service('userBasicInfo', function () {
    ship = {};
    billing = {};
  })
  .factory('bestDealsData', function () {
    return {
      productList: {}
    }
  })
  .factory("transformRequestAsFormPost", function () {
    // I prepare the request data for the form post.
    function transformRequest(data, getHeaders) {
      var headers = getHeaders();
      headers["Content-type"] = "application/x-www-form-urlencoded; charset=utf-8";
      return (serializeData(data));
    }
    // Return the factory value.
    return (transformRequest);
    // ---
    // PRVIATE METHODS.
    // ---
    // I serialize the given Object into a key-value pair string. This
    // method expects an object and will default to the toString() method.
    // --
    // NOTE: This is an atered version of the jQuery.param() method which
    // will serialize a data collection for Form posting.
    // --
    // https://github.com/jquery/jquery/blob/master/src/serialize.js#L45
    function serializeData(data) {
      // If this is not an object, defer to native stringification.
      if (!angular.isObject(data)) {
        return ((data == null) ? "" : data.toString());
      }
      var buffer = [];
      // Serialize each key in the object.
      for (var name in data) {
        if (!data.hasOwnProperty(name)) {
          continue;
        }
        var value = data[name];
        buffer.push(
          encodeURIComponent(name) +
          "=" +
          encodeURIComponent((value == null) ? "" : value)
        );
      }
      // Serialize the buffer and clean it up for transportation.
      var source = buffer
        .join("&")
        .replace(/%20/g, "+")
        ;
      return (source);
    }
  }
  );
